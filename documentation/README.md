# GGCGen-documentation

GGCGen-documentation is an effort to document the software GGCGen.

## GGCGen

GGCGen - a Graphical G-code Generator and robot control software.
This program provides a graphical user interface (GUI) for easy 
creation of methods (scripts) for controlling cartesian robots - also known as 
lab robots, liquid handling robots or pipetting robots. GGCGen provides a 
variety of predefined working space layouts and customizable method blocks for 
point-and-click creation of (partially opentrons API v2-compatible) 
protocols that are executed by the robot.
 
The intuitive interface guides the user through the process of protocol design.
Experts can use the built-in editor to modify or extend the semi-automatically 
created python script. Together with the opentrons to G-code converter 
[pyotronext](https://gitlab.com/RobotsByDerAndere/pyotronext/tree/master), 
the script can be directly run on robots with Marlin 2.0 firmware.


## License information

```
"""
SPDX-License-Identifier: Apache-2.0 OR CC-BY-4.0
GGCGen-documenation
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
See AUTHORS file (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
"""
```

GGCGen-documentation (This work) is dual-licensed under the terms of the
Apache License, Version 2.0, or the 
Creative Commons Attribution 4.0 International License, at Your option 
(Apache-2.0 OR CC-BY-4.0).
For detailled license information, see .dep5 file, and the [./LICENSES/ folder](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/src/documentation/LICENSES) folder at the root directory of this work. This project uses the [REUSE framework](https://reuse.software) to simplify license compliance.


You may choose one of the following two options: 

[
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues).
This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this 
license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to 
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
]

OR:

[ 
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 
]

