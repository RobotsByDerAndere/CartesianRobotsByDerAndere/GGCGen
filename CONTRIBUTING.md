# Developer Certificate of Origin + License

By contributing to GGCGen by DerAndere and other GGCGen authors, You accept and 
agree to the following terms and conditions for Your present and future 
contributions submitted to the GGCGen project. Except for the license granted 
herein to DerAndere and recipients of software distributed by DerAndere, You 
reserve all right, title, and interest in and to Your contributions. All 
contributions are subject to the following DCO + License terms.

https://gitlab.com/RobotsByDerAndere/DevOriginCertByDerAndere/blob/master/README.md


# Security vulnerability disclosure

Please report suspected security vulnerabilities in private to
DerAndere, also see the disclosure section in the file ./README.
Please do NOT create publicly viewable issues for suspected security
vulnerabilities.
You can create a new issue at 
https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues, 
set it as confidential and give it the title SecurityIssueX where X is replaced 
by a number. The Authors of this software will contact you by e-mail, then.


# Code of conduct

As contributors and maintainers of this project, we pledge to respect all
people who contribute through reporting issues, posting feature requests,
updating documentation, submitting pull requests or patches, and other
activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct. Project maintainers who do not
follow the Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported to the GGCGen maintainer DerAndere e.g. by creating an issue at 
https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues, 
set as confidential and give it the title ConfidentialIssueX where X is 
replaced by a number.

This Code of Conduct is adapted from the [Contributor Covenant, version 1.1.0](http://contributor-covenant.org/version/1/1/0/).


