; Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
; Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
; SPDX-License-Identifier: Apache-2.0
;
; G-code script for controlling a cartesian robot (also known as lab robot,
; liquid handling robot or pipetting robot) that runs marlin firmware
; (http://marlinfw.org/meta/download/).
; This script was (partially or as a whole) generated semi-automatically using
; the software GGCGen
; (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/).
; GGCGen was developed in 2018 by DerAndere and other GGCGen authors 
; (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
; and is licensed under the terms of the 
; Apache License, Version 2.0.

O1000 ; Program sequence number 
; This preamble is mandatory. Keep the preset text at the beginning of the G-code script unchanged 
M302 S0 ; Allow cold extrusion / dispensing at all temperatures 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
; End of the mandatory preamble. Keep above preset text unchanged. User-defined G-code script is amended below. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ; initialization 
 G1 Z230 F300 ; Retract tool to prevent collision with obstacles 
 G1 X0 Y0 Z230 F300 ; Move to secure-start position (0/0/210) 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

