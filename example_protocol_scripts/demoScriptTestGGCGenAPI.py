"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/example_protocol_scripts/demoScriptTestGGCGenAPI.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors, see GGCGen AUTHORS file
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a G-code generator and robot control software. 
This file contains a Python script that generates the specified G-code script.  
Language: Python 3.7
"""


import pyotronext.export
from pyotronext import protocol_api


run(protocol: protocol_api.ProtocolContext):
    plate_1 = protocol.load_labware(load_name="corning_96_wellplate_360ul_flat", slot=1)
    tipRack10ul_2 = protocol.load_labware(load_name="gep_96_tiprack_10ul", slot=3)
    pipette = protocol.load_instrument(load_name = "p10_single", mount="left", tip_racks=[tipRack10ul_2])

    pipette.move_to(plate_1["A1"])
    pipette.move_to(plate_1["A2"].middle())
    pipette.aspirate(10)
    pipette.dispense(plate1["A3"])


    pipette.teardown()

filename = "testfile.py"  # save the filename that was actually specified for export in the dialog.
dirname = pathlib.Path("C:/testfolder/")  # looks for the the path to the file that was actually specified for export.
file = dirname / filename  #creates a path recognizeable by the currently running operating system and opens the previously specified file in mode="w" (open for writing, truncating existing file first. creating the file if it did not exist. 

protocol = pyotronext.execute.get_protocol_api(direct_write=False, 
                                              outfile=pathlib.Path("C:/testfolder/")/"testfile.py"
                                              version="2.0")

run(protocol)


