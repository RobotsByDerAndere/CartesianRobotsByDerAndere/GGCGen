# GGCGen

GGCGen - a Graphical G-code Generator and robot control software.
This program provides a graphical user interface (GUI) for easy 
creation of methods (scripts) for controlling cartesian robots - also known as 
lab robots, liquid handling robots or pipetting robots. GGCGen provides a 
variety of predefined working space layouts and customizable method blocks for 
point-and-click creation of (partially opentrons API v2-compatible) 
protocols that are executed by the robot.
 
The intuitive interface guides the user through the process of protocol design.
Experts can use the built-in editor to modify or extend the semi-automatically 
created python script. Together with the opentrons to G-code converter 
[pyotronext](https://gitlab.com/RobotsByDerAndere/pyotronext/-/tree/master), 
the script can be directly run on robots that understand G-code 
(a dialect of ISO 6983-1 and ANSI/EIA RS274-D G-code). Compatible controller firmware:
- [Marlin2ForPipetBot](https://github.com/DerAndere1/Marlin), 
- [Marlin firmware](https://github.com/MarlinFirmware/Marlin),
- [grblHAL](https://github.com/grblHAL/core),
- [Smoothie](https://github.com/Smoothieware/Smoothieware),
- [Duet3D RepRap firmware](https://github.com/Duet3D/RepRapFirmware),
- [Synthetos g2core](https://github.com/synthetos/g2),
- [Repetier Firmware 2.0](https://github.com/repetier/Repetier-Firmware/tree/dev2),
- [LinuxCNC](https://linuxcnc.org/)


# Getting started - Find help

This README contains information on how to install and use GGCGen, see 
below. The [user manual can be found online](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/wikis/GGCGen-User-Manual) 
as part of the [GGCGen Wiki](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/wikis/wiki_GGCGen_home).


## Installation:

1. Install a Python 3.7 or Python 3.8 interpreter that comes with all distributions of Python. 
For Windows, I recommend CPython that is part of the [reference Python 
distribution by the Python Foundation](https://www.python.org/downloads/) 
(an executable installer is provided for download). Run the installer by double-
clicking the executable file you downloaded. Follow the instruction on the 
screen and when the installer wizard dialog gives the option to "Install Python 
in Path" and "install Python launcher for Windows", select both options. 

2. For the installation of Python packages, I recommend to use the tool pip by 
the Python Packaging Authority (PyPa) which is included in the Python 3.8 
distribution of the Python Foundation (if you don't have pip installed, follow 
[the instructions](https://pip.pypa.io/en/stable/installing/)). 
Then navigate to the powershell.exe and right-click the executable powershell.exe. In the
context  menu that appears, left-click "Run as Administrator". 
Type the following in the command prompt and confirm by pressing the 
enter-key:
```
py -3.8 -m pip install -r https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/-/archive/master/GGCGen-master.tar.gz
```

This will install first all dependencies listed in the [requirements.txt file](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/-/blob/master/requirements.txt) and finally GGCGen itself in
`Path\to\Python38\Lib\site-packages\ggcgen`.

Finally, you may want to edit the systen environment variables to add ggcgen to the System Path environment variable.
To do so, 

1. Right-click on the start menu (Hotkey: \[Windows key + x\]) to access the Power User Task Menu.
1. In the Power User Task Menu, left-click the option "System".
1. In the About window, click the Advanced system settings link under Related settings on the far-right side.
1. In the System Properties window, click the Advanced tab, then click the Environment Variables button near the bottom of that tab.
1. In the Environment Variables window, highlight the Path variable in the System variables section and click the Edit button. 
1. Select the last element in the list and Left-click "New"
1. In the text-field, enter the path to the parent directory of ggcgen.py, e.g. `C:Python38\Lib\site-packages\ggcgen` and confirm by pressing the \[Enter\] key
1. Left-click "OK" and again "OK" and close the About window.


### Configuration of GGCGen

Open the following files with a program 
that can edit text files (*txt) and edit the parameters to fit your 
hardware:

- Robot specifications, general configuration: .../ggcgen/config.ini
   (uses configObj format, see defaultLayout.ini)
- Specifications of labware: ggcgen/labware/ (uses JSON format 
  that is used by opentrons Inc. to specify labware for the opentrons framework)
- Spacial arrangement of labware inside the working space: 
   ggcgen/layouts/ . These .INI files use a format that is 
   compatible with configObj.The sections should be called [labwareX] with 
   incrementing X in steps of 1 (see defaultLayout.INI).


## Running GGCGen

After GGCGen and its dependencies are installed and configured as described in 
the section "Installation" above, you can start the graphical user interface of GGCGen by do the following:

### Method 1:

1. Navigate to the parent directory of ggcgen.py which has the name ggcgen and is located in ...Python38/Lib/site-packages. 
1. Right-click the file ggcgen/ggcgen.py and 
1. in the context menu that appears, 
left-click "Open with..." -> "Select app". 
1. Browse your computer and navigate to the Python interpreter. For Windows, I recommend CPython which should be in the 
directory .../Python38/python.exe after installing Python 3.8 (see above). 
Select the option to remember to use the python.exe as the standard 
application to open Python scripts (files with file extension *.py).
 
From now on, you can run GGCGen by double-clicking the file ...ggcgen/ggcgen.py.
You may create a short-cut to the file ...ggcgen/ggcgen.py and place it in a
convenient location on your computer.

### Method 2:

After you have registered python.exe as the default program to open *.py files (see method 1), you can also run GGCGen by 
executing the following from the PowerShell: 
```
cd "<Path\to\Python38>\Lib\site-packages\ggcgen"
```
followed by (with or without file extension)
```
ggcgen.py
```

### Method 3a:

If you had edited the system path environment variable to add ggcgen to the system path, you do not need to change the current work directory with `cd`.
You can simply run powershell.exe and execute the following, regardless of the current working directory:
```
Start-Process -FilePath "ggcgen.py"
```
or use the last method presented:


### Method 3b

Enter in the PowerShell (again, with or without file extension):
```
ggcgen.py
```

This will run GGCGen and display it's GUI. The user manual can be opened from within GGCGen by left-clicking the menu
"Help" -> "User Manual".


## Creating methods/protocols for the PipetBot-A8 - Generating G-code scripts

The main frame/window contains a main menu bar, a tool bar and below that, you
find the method creation area which is subdivided into 2 tabs (pages):

- Set Next Command
- Method Overview


### The tab "Set Next Command" 

The default tab that is opened when starting GGCGen is "Set Next Command". 
Click on the controls in the method creation area. Each control creates a
specific G-code method block that is appended at the end of the method.
Clicking on the buttons
representing items in the working space of the robot adds a method block 
to the method that will send G-code commands for xy-movement to the specified 
item once the script is run. To change the workingspace 
layout, left-click "Change layout - open wizard" (currently broken). To 
add labware, left-click the "Add Labware" button 
Customized method blocks can be added to the method by left-clicking on the 
buttons "Dispense - open wizard", "Move xy - open wizard", "move z" (after 
typing in the z-position in the respective textbox. This command is agnostic of 
the attached tool and refers to the raw z position as defined by the firmware). 
Left-clicking the button "move tip to predefined z - open wizard" allows to 
choose between predefined z-positions. In opposition to the command "move z" 
(see above), this smart command keeps track of the current tool (e.g. tip hight 
and tool z-offset) and sets the position of the very tip of the attached tool. 
Predefined special commands can be issued by left-clicking on one the respective 
list-element in the ListBoxControl at the top right corner. 

After all desired method blocks were added, continue with the tab "Method 
Overview". 


### The tab "Method Overview"

The tab Method Overview provides an overview of the method consisting of
the method blocks issued before. Modify the text in order to change details.
Make sure the changes result in valid G-code and do not result in damage. 
It is also possible to use text editing to delete/move/duplicate method blocks.
After verifying the resulting method, it can be exported and / or executed
as described in the following sections.


## Executing the protocol from within GGCGen

"Method Overview" --> "Execute script" 

Left-click to run the current method from within GGCGen. This saves the method 
in a temporary Python script (including preamble, run(protocol) and teardown) 
and runs it. 


## Export as opentrons OT-2 App compatible Python protocol

"Method Overview" -> "Export OT-2 App compatible"

Left-click to export the method / protocol as Python script in a format that can 
be run on an opentrons-compatible robot from within the opentrons OT-2 App. 


## Exporting a Python script for execution from the commandline

"Method Overview" --> "Export Python script"

Left-click to save the current method including preamble, definition of the 
run() function containing the protocol, followed by run(protocol) and 
teardown() as a Python script that can be executed with a Python interpreter 
from the commandline. If set up correctly, double-clicking on the exported 
Python script file should also invoke the Python interpreter to run the 
protocol. The file contains, amongst other content, the definition of the run() 
function that can be copied into a new python script for use with the opentrons 
software framework. 


## Executing the method / protocol from 3rd party G-code senders / host software

"Method Overview" --> "Export G-code"

Left-click to export the current method as G-code file which can be executed 
using third-party host software like [Printrun/Pronterface](http://www.pronterface.com/). 
This exports a text file containing the G-code script with .gcode file 
extension. Detailed instructions are available on the
[Pipetbot-A8 project page](https://derandere.gitlab.io/pipetbot-a8).


# Execution of opentrons protocols (opentrons API v2 compatible Python scripts)

You can use GGCGen to interpret Python scripts that are compatible with opentrons API v2 (e.g. Python protocols 
for original Opentrons robots) and directly control CNC / robot hardware accoringly.

To do so, set the baudrate in the file config.ini to the data transfer rate in baud = bits per minute that is 
expected by the CNC / robot firmware.
Then, create a new Python script with the content of the template below and replace 
the definition of the `run` with the one from the original protocol. 
```
import pyotronext.execute
from pyotronext import protocol_api 

def run(protocol: protocol_api.ProtocolContext):
    # the contents of your protocol are here...
    pipette.teardown()  # change pipette to the name of your instrument
protocol = pyotronext.execute.get_protocol_api(version="2.0")
    
run(protocol)

```

The above code works as follows: 
`pyotronext.execute.get_protocol_api()`. Creates a virtual representation
of the lab working space to be controlled. The arguments of this function 
change the parameters changing behaviour:

@param `version`: `str`, api version

After saving, you can execute the protocol by double-clicking on the file or by running cmd.exe and 
entering the following command: 
python "Drive:Path/To/ProtocolDirectory/protocolName.py"

This will cause GGCGen's backend pyotronext will take care of all the following steps behind the scenes:
It will convert the protocol to G-code (ISO 6983-1 or ANSI/EIA RS274-D standard compliant) communicating with the CNC controller via USB using the 8-N-1 
asynchronous serial communication protocol (8 data bits, no parity, 1 stop bit) at the set baudrate. 

Compatible controller firmware:
- [Marlin2ForPipetBot](https://github.com/DerAndere1/Marlin), 
- [Marlin firmware](https://github.com/MarlinFirmware/Marlin),
- [grblHAL](https://github.com/grblHAL/core),
- [Smoothie](https://github.com/Smoothieware/Smoothieware),
- [Duet3D RepRap firmware](https://github.com/Duet3D/RepRapFirmware),
- [Synthetos g2core](https://github.com/synthetos/g2),
- [Repetier Firmware 2.0](https://github.com/repetier/Repetier-Firmware/tree/dev2),
- [LinuxCNC](https://linuxcnc.org/)

# Known Issues

- Special commands are broken
- No error checking / no checks for invalid user input in textboxes.
   Make sure to enter text in the requested format only and do not exceed 
   limits (e.g. regarding min/max values for x, y z, volume (E) and speed (F)) 
   described in the specifications of your hardware. 
- When selecting multiple cells, you always have to press the Ctrl key before 
  starting selections, otherwise the first selection will be additionally recognized 
  as a single selection. 

The following controls do not work:

- File -> New          !!! workaround provided below *
- File -> Load/Open    !!! workaround provided below **

* workaround for creating a new method:
1. Run a new instance of GGCGen by opening GGCGen.py with a Python interpreter.


** workaround for loading and editing an existing method:
1. Open the G-code file that contains the method in a program that can open 
   and display text files (*txt)
2. Run GGCGen.py 
3. In GGCGen, left-click "Method Overview"
3. Copy the content of the file opened in step 1, excluding the preamble, into
   the textbox in the method creation area of the GGCGen tab "Method Overview"
4. Edit the contents of the textbox in the Method Overview tab
5. Place the cursor at the end of a method block in the textbox
6. Optionally, add further method blocks at the cursor position using the tab 
   "Set Next Command".


# Deinstallation:

In order do deinstall GGCGen, simply delete the folder "GGCGen" that you 
downloaded as described in the section "Download GGCGen" above. If you have 
created your own short-cuts to the contents of that folder, you may want to
delete these, too. All dependecies can then be deinstalled if you like. Python 
packages installed with pip can be deinstalled by navigating to the powerpoint.exe and 
right-clicking the executable cmd.exe. In the context  menu that appears, 
left-click "Run as Administrator". Type the following in the command prompt and 
confirm by pressing the \<enter\>-key after each line:
```
py -3.8 -m pip uninstall "ggcgen"
py -3.8 -m pip uninstall "pyotronext"
py -3.8 -m pip uninstall "wxPython"
py -3.8 -m pip uninstall "configobj"
```
Similarly, you can uninstall the package "wheel" and indirect dependencies (six, pillow, numpy) using pip, but we recommend to keep them as well as Python itself installed.
If you do not need Python anymore, it can be deinstalled by following the instructions in the Python 
documentation.


# Customizing and improving GGCGen  

Read the [contribution guidelines for the GGCGen project](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/CONTRIBUTING.md). 
(see file ./CONTRIBUTING.md). Issues (bugs and feature requests) with a fresh
installation of the latest GGCGen from https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen 
can be reported at [the issue-tracker for GGCGen](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues). 
Developers who want to improve GGCGen or who want to create their own working 
space layouts can fork the git repository 
git@gitlab.com:RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen.git or modify
the files in the downloaded local GGCGen program folder. People that are new to 
git can do so by creating an account at e.g. [GitLab.com](https://gitlab.com), 
visiting the [project page of GGCGen](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen) and clicking 
on the control panel "Fork" and specifying a target namespace for the fork.
Please notice the information in the file ./LICENSE. 
Contributing back to the original GGCGen project is highly welcome. Please send 
pull requests or merge requests from a fork via git to 
git@gitlab.com:RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen.git or 
[file an issue and add patches as attachments to the 
bug report / feature request or in the comments](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues).


## Code structure

The source code is structured as in the table below. Files that need to be 
edited by users for configuration of their robot are _emphasized_, files that
are important for developers are shown with **strong emphasis**: 

| Subfolder     | File             | Class                 | Description            |
|:--------------|:-----------------|:----------------------|:-----------------------|
|src/resources  |Quit.png          |NA                     |icon symbol             |
|src/ggcgen/    |**_config.py_**   |NA                     |robot dimensions. Users should adjust this|
|.../           |**ggcgen.py**     |GGCGenWindowFrame      |main loop and main wx.frame containing menus and a wx.notebook instance. Page (tab) 0 is an instance of GGCGenMethodOverviewPanel.PanelMethodOverview, page 1 is an instance of GGCGenNextCommandPanel.PanelSetNextCommand.|
|.../           |dialogs.py        |About_Dialog           |info on version, credits, license|
|.../           |...               |ExportConfigDialog     |exporting G-code to .gcode text file|
|.../           |...               |Set_Layout_Dialog      |choose work space layout to be dislayed|
|.../           |...               |Move_xy_Dialog         |writes mecode command block for "move xy" to PanelMethodOverview.mecodeTextCtrl|
|.../           |...               |Dispense_Dialog        |" " " " "move e" " "|
|.../           |...               |Move_Tip_to_Predef_z...|" " " " "move z" " "|
|.../           |...               |NA                     |" " " " "move z" " ", special commands|
|.../           |layout.py         |PanelLayout, Layout    |Panel representing working space containing instances of classes from plate.py|
|.../           |post_process.py   |GGCGenMethodOverviewPanel|mecodeTextCtrl (an instance of wx.TextCtrl) for recording mecode method blocks and buttons for export and execution|
|.../           |**creator.py**    |GGCGenNextCommandPanel |panel containing instances of classes from layout.py and plate.py that display layout and labware|
|.../           |**plate.py**      |PanelPlate,            |labware representation on screen as wx.Panel and interaction via instance of wx.grid.Grid)|
|.../           |labwareItems.py   |LabwareItems, Vials    |general representation of labware dimensions as Python object|
|.../           |robot.py          |VirtualRobot           |representation of robot configuration and tracking of the robot state|
|.../           |utils.py          |NA                     |functions to get parent directory and path to resources|
|.../layouts/   |_[name].INI_      |NA                     |layout of labware on the working space in configObj compatible INI format|
|.../labware/   |_[load_name].JSON_|NA                     |labware definitions in opentrons JSON format|
|               |AUTHORS           |NA                     |List of authors for copyright purposes|
|               |CONTRIBUTING      |NA                     |Contribution guidelines for the GGCGen project|
|               |LICENSE           |NA                     |license with copyright information|
|               |NOTICE            |NA                     |NOTICE for compliance with 3rd-party components licensed under the Apache License, Version 2.0|


## Distributing GGCGen

1. Update all version numbers and push the changes
1. Create a tag for the last commit (e.g. tag: 1.0.1, message: "tagged release of GGCGen v1.0.1" and push the changes
1. Download https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/-/archive/master/GGCGen-master.tar.gz and extract the archive.
1. run powershell.exe and enter the following command to change directory to the parent directory of the setup.py file within GGCGen:
```
cd "Path\to\parentdirectory\GGCGen"
```
To create a wheel (.whl file) ind the dis subdirectory, enter 
```
py -3.8 setup.py bdist_wheel
```
1. log in to gitlab.com
1. go to https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/-/releases
1. Left-click "New Release". Release name: v1.0.1, release notes: tagged release of GGCGen 1.0.1 and attach the wheel (*.whl) from above. 
1. Add the wheel to the artifacts by adding a URL to the wheel in the section below the release notes.
1. Left-click "OK"


## Credits

Thanks to:

- DerAndere: Initial design and implementation, core developer, current maintainer. ([Donate](https://www.paypal.com/donate/?hosted_button_id=TNGG65GVA9UHE))
- Opentrons Labworks, Inc.: labware and design of the protocol format
- Other contributors: See [file ./AUTHORS](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen). 
- For individual contributions: See [file ./CHANGELOG](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/) 
   and the history of the git repository (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/commits/master)


# License information

```
"""
SPDX-License-Identifier: Apache-2.0

GGCGen (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
See AUTHORS file (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)

This work is

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

This project uses the [REUSE framework](https://reuse.software) to simplify license compliance. For details, see the ./.reuse/*.dep5 file and the [./LICENSES/ folder](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/LICENSES) in the root directory of this work.


This software includes software developed by DerAndere:

Contributions by DerAndere
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues).


This software includes software developed by The GGCGen authors:

Contributions by The GGCGen authors
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS).



This software contains software derived from portions of the Opentrons 
Platform 3.15.2 (parts of opentrons/api/*) by Opentrons Labworks, Inc., with 
various modifications by DerAndere and other GGCGen authors, 
see GGCGen AUTHORS file 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS):

Opentrons Platform 3.15.2 (https://www.github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks, Inc.

"""
```
