"""
Copyright 2018 - 2020 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2020 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/tests/test_main.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright  2018 - 2020 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2020 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains tests.  
Language: Python 3.7
"""

import pytest

def test_file1_method1():
    x=5
    y=6
    assert x+1 == y,"test failed"
def test_file1_method2():
    x=6
    y=6
    assert x == y,"test failed" 

