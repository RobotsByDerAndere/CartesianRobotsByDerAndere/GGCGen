"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/layout.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains the classes Layout and PanelLayout.  
Language: Python 3.7
"""

import json
import pathlib

import wx  # Python wrapper for wxWidgets (https://pypi.org/project/wxPython/ ). provides classes and methods for GUI development such as wx.EventHandler and the classes that inherit from it (wx.Window, wx.Notebook, wx.Frame, wx.Dialog, wx.Panel), classes for controls such as wx.Controls and wx.Button and event objects such as wx.CommandEvent which emits events such as EVT_BUTTON.
import configobj

import utils
import configurator
import labwareItem

allCenterBorder5Flags = wx.SizerFlags().Align(wx.ALIGN_CENTER).Border(wx.ALL, 5).Proportion(0)  # wx.SizerFlags are the preferred way for setting sizers


"""
Classes for the Protocol creator reading chosen layoutfile
->adding PlatePanels to GUI and creating load_labware strings 
"""


class Layout:
    def __init__(self, customLayoutFilePath=None, **kwargs): 
        super().__init__(**kwargs)  # implicit inheritance using super is superior to explicit inheritance
        self.currentScriptDirectory = utils.getParentDirectory()
        self.definitionDirectory = self.currentScriptDirectory / "labware" 
        self.config = configurator.Config.config    
        self.layoutChoice = "defaultLayout"
        self.layoutFileName = self.layoutChoice + ".INI"
        self.customLayoutFilePath = customLayoutFilePath
        if self.customLayoutFilePath is not None:
            self.layoutFilePath = self.customLayoutFilePath
        else:
            self.layoutFilePath = utils.getResourcePath("layouts", self.layoutFileName)

        try: 
            with self.layoutFilePath.open() as layoutFileHandler:
                self.currentLabwareConfigObj = configobj.ConfigObj(infile=layoutFileHandler, unrepr=True)
        except OSError:
            raise wx.LogError("cannot open file")
#        self.labwarePaths = [(currentScriptDirectory / ("labware/" + lbwre["type"] + ".json")) for lbwre in currentLabwareConfigObj.section]
#        self.labwareDefs = [json.load(self.labwarePaths[labwre]) for labwre in self.labbwarePaths]
#        self.currentLabwareDict = {lbwr["type"]+str(lbwr["IDnumber"]): plate.LabwareItem(definition = json.load(labwareDirectory / (lbwr["type"] + ".json")), IDnum = lbwr["IDnumber"], slot=lbwr["slot"]) for lbwr in self.currentLabwareConfigObj.sections}
#        self.currentLabwareDefinitions = []

        self.labwareObj = {}
        self.wasteObj = []
        self.loadLabwareFromLayoutFile(labwareConfigObj=self.currentLabwareConfigObj)
        
    def loadLabwareFromLayoutFile(self, labwareConfigObj=None):
        definitionFilePaths = []
        currentLabwareDefinitions = []
        if labwareConfigObj is not None:
            for lbwrIdx in range(0,len(labwareConfigObj.sections)):
                definitionFilePaths.append(self.definitionDirectory / (labwareConfigObj["labware" + str(lbwrIdx + 1)]['type'] + ".json")) 
                try:
                    with definitionFilePaths[lbwrIdx].open() as definitionFileHandler:
                        data = json.load(definitionFileHandler)
                except OSError:
                    wx.LogError("cannot open file")
                currentLabwareDefinitions.append(data)
                self.addLabware(definition=currentLabwareDefinitions[lbwrIdx], IDnum=labwareConfigObj["labware" + str(lbwrIdx + 1)]["IDnumber"], slot=labwareConfigObj["labware" + str(lbwrIdx + 1)]["slot"])

    def addLabware(self, definition, IDnum, slot):
        self.labwareObj.update({definition["parameters"]["loadName"] + "_" + str(IDnum): labwareItem.LabwareItem(definition = definition, IDnum = IDnum, slot = slot)})
        if definition["parameters"]["format"] == "trash":
            self.wasteObj.append(labwareItem.LabwareItem(definition = definition, IDnum = IDnum, slot = slot))

class PanelWorkspace(wx.Panel):
    """Panel that represents the working space. This panel represents the 
    standard layout."""    
    def __init__(self, parent=None, **kwargs): 
        super().__init__(parent=parent, **kwargs)  # implicit inheritance using super is superior to explicit inheritance
        self.layout = Layout()
        self.nextID = len(self.layout.currentLabwareConfigObj.sections)+1
        self.workspaceStaticBox = wx.StaticBox(parent=self, label = "Click vessel in workspace layout to move in xy ", size = ( self.layout.config["xSizeWorkingSpace"] * 2, self.layout.config["ySizeWorkingSpace"] * 2 ))
        self.workspaceStaticBoxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.workspaceStaticBoxSizer.Add(self.workspaceStaticBox, proportion=0, flag=wx.ALL, border=0)

class PanelLayout(wx.Panel):
    """Panel that represents the working space. This panel represents the 
    standard layout."""    
    def __init__(self, parent=None, layoutFilePath=None, **kwargs): 
        super().__init__(parent=parent, **kwargs)  # implicit inheritance using super is superior to explicit inheritance
        self.layout = Layout(customLayoutFilePath=layoutFilePath)
        self.nextID = len(self.layout.currentLabwareConfigObj.sections)+1
        self.workspaceStaticBox = wx.StaticBox(parent=self, label = "Click vessel in workspace layout to move in xy ", size = ( self.layout.config["xSizeWorkingSpace"] * 2, self.layout.config["ySizeWorkingSpace"] * 2 + 10))
        self.workspaceStaticBoxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.workspaceStaticBoxSizer.Add(self.workspaceStaticBox, proportion=0, flag=wx.ALL, border=0)
        self.platePanels={}
