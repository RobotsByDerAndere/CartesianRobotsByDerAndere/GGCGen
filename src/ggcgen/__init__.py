"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/__init__.py
@package ggcgen
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
Create and execute methods for the 3D-printer-based DIY liquid handling robot 
PipetBot-A8 by semi-automatically generating Opentrons APIv2 protocols and 
turning APIv2 protocols into G-code scripts using a graphical user interface.
Inspired by the Opentrons software framework by Opentrons Labworks, Inc. 
See https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/README.md . 
Language: Python 3.7
Status: Beta
@version 1.0.7
"""

__author__      = "DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)"
__copyright__   = "Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)"
__license__     = "Apache-2.0"
__version__     = "1.0.7"
