"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/ggcgen.py
@package ggcgen
@authors 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details ggcgen is a Graphical G-code generator and robot control software. 
This file is the main python script. It contains the main loop (entry point)
which with the class GGCGenApp (a wxPython app providing a GUI) 
Inspired by the opentrons API version2 Python package by Opentrons Inc. 
See https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/README.md . 
Language: Python 3.7
status: Beta
@version 1.0.6
"""

import webbrowser  # provides webbrowser.open() for opening URL from hyperlinks given as string
import pathlib  # provides Path() and Path().write_text(), Path().resolve() and Path().resolve().Parent() for file handling. Replaces os.path and does not need open()

import wx  # Python wrapper for wxWidgets (https://pypi.org/project/wxPython/ ). provides classes and methods for GUI development such as wx.EventHandler and the classes that inherit from it (wx.Window, wx.Notebook, wx.Frame, wx.Dialog, wx.Panel), classes for controls such as wx.Controls and wx.Button and event objects such as wx.CommandEvent which emits events such as EVT_BUTTON.

import utils
import dialogs
import creator
import post_process
import robot
import configurator

class GGCGenWindowFrame(wx.Frame):
    """Main window/frame of ggcgen to which all GUI elements are appended
    at initialization (when ggcgen.py starts to run)"""

    def __init__(self, **kwargs):
        super().__init__(size = (1024,800), **kwargs)
        self.InitUI() 


    def InitUI(self):
        """main menu bar containing file menu and help menu"""

        
        mainMenuBarInitUI = wx.MenuBar()

        fileMenu = wx.Menu()  # menu File
        fileMenuNew = fileMenu.Append(id=wx.ID_NEW, item="&New")  # menu File -> New; &x makes key x hotkey
        filemenuOpen = fileMenu.Append(id=wx.ID_OPEN, item="&Open")  # menu File -> Open
        fileMenuSave = fileMenu.Append(id=wx.ID_SAVE, item="&Save")  # menu File -> Save
        fileMenu.AppendSeparator()

        fileMenuSubMenuImport = wx.Menu()  # menu File -> Import
        fileMenuSubMenuImportItemImportGcode = fileMenuSubMenuImport.Append(id=wx.ID_ANY, item="Import G-code", helpString="Import G-code from textfile")
        fileMenuSubMenuImportItemImportScript = fileMenuSubMenuImport.Append(id=wx.ID_ANY, item="Import GGCGenMethod file", helpString="Import GGCGenMethod file")  
        fileMenu.AppendSubMenu(submenu=fileMenuSubMenuImport, text = "&Import", help = "Import method")        

        fileMenuItemQuit = fileMenu.Append(id=wx.ID_EXIT, item="Quit", helpString="Exit / Quit application")


        self.Bind(event=wx.EVT_MENU, handler=self.onQuit, source=fileMenuItemQuit)
        self.Bind(event=wx.EVT_MENU, handler=self.onImportGcode, source=fileMenuSubMenuImportItemImportGcode)
        self.Bind(event=wx.EVT_MENU, handler=self.onImportScript, source=fileMenuSubMenuImportItemImportScript)
        self.Bind(event=wx.EVT_MENU, handler=self.onSave, source=fileMenuSave)

        helpMenu = wx.Menu()  # menu Help
        helpMenuItemManual = helpMenu.Append(id=wx.ID_HELP, item="&Manual", helpString="Help - user manual")  # menu Help -> Manual
        helpMenu.AppendSeparator()
        helpMenuItemAbout = helpMenu.Append(id=wx.ID_ABOUT, item="&About", helpString="About ggcgen - version information, license and credits")  # menu Help -> About

        self.Bind(event=wx.EVT_MENU, handler=self.onAbout, source=helpMenuItemAbout)
        self.Bind(event=wx.EVT_MENU, handler=self.onManual, source=helpMenuItemManual)


        mainMenuBarInitUI.Append(menu=fileMenu, title="&File")
        mainMenuBarInitUI.Append(menu=helpMenu, title="&Help")
        self.SetMenuBar(mainMenuBarInitUI)
        
        
        """toolbar"""
        toolbar1InitUI = self.CreateToolBar()
        toolbar1InitUIitemQuit = toolbar1InitUI.AddTool(toolId=1, label="Quit", bitmap=wx.Bitmap(utils.getResourcePathString("Quit.png")))  # If the application is rootfolder/applicationmain.py together with rootfolder/__init__.py, then the image has to be in rootfolder/resources/  
        toolbar1InitUI.Realize()

        self.Bind(event=wx.EVT_TOOL, handler=self.onQuit, source=toolbar1InitUIitemQuit)


        """Notebook panel providing pages (tabs) for the sections 
        PanelSetNextCommand ('Set Next Command'), PanelMethodOverview ('Method 
        overview') and PanelFileGenerator ('G-code file export')"""
        self.notebookInitUI = wx.Notebook(parent=self)
        self.notebookPagePanelMethodOverview = post_process.PanelMethodOverview(parent=self.notebookInitUI)
        self.notebookInitUI.InsertPage(index=0, page=self.notebookPagePanelMethodOverview, text="Method Overview", select = False)
        self.notebookPagePanelSetNextCommand = creator.PanelSetNextCommand(parent=self.notebookInitUI)
        self.notebookInitUI.InsertPage(index=0, page=self.notebookPagePanelSetNextCommand, text="Set Next Command", select = True) 
        self.Centre()


    def onQuit(self, quitEvent):
        self.Close()

    def onImportGcode(self, importEvent):
        print("ImportGcode not implemented")

    def onImportScript(self, importGGCGenMethodEvend):
        print("ImportGGCGenMethod not implemented")

    def onAbout(self, aboutEvent):
        """Open About_Dialog ('About ggcgen')"""
        onAboutDialog = dialogs.About_Dialog(parent=self, title="About ggcgen")
        onAboutDialog.ShowModal()
        onAboutDialog.Destroy

    def onManual(self, manualEvent):
        """Open the Link to the online manual at 
        https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/README.md"""
        wx.BeginBusyCursor()
        webbrowser.open("https://www.gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/README.md")
        wx.EndBusyCursor()

    def onSave(self, saveEvent):
        """Open wx.FileDialog and save Python/mecode script as text file with file 
        extension .py. The content is a concatenation of the 
        preamble and the content of the wx.TextCtrl gCodeTextbox of the 
        PanelFileGenerator."""  
        onExportMecodeToFileBtnConfigDialog = GGCGenDialogs.ExportConfigDialog(parent=self, title = "Export Config Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if onExportMecodeToFileBtnConfigDialog.ShowModal() == wx.ID_OK:
            printerPort = onExportMecodeToFileBtnConfigDialog.inputTextCtrlSerialPort.GetValue()
            onExportMecodeToFileBtnDialog2 = wx.FileDialog(parent=self, message="Choose a file", defaultDir=str(pathlib.Path.cwd()), defaultFile="", wildcard="*.py", style=(wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT), name = "Export mecode method as Pythonscript")
            if onExportMecodeToFileBtnDialog2.ShowModal() == wx.ID_OK:
                filename = onExportMecodeToFileBtnDialog2.GetFilename()  # save the filename that was actually specified for export in the dialog.
                dirname = pathlib.Path(onExportMecodeToFileBtnDialog2.GetDirectory())  # looks for the the path to the file that was actually specified for export and creates a pathlib.path object 
                file = dirname / filename  #creates a path (an object of type pathlib.path) that is recognizeable by the currently running operating system using pathlib functionality 
                file.write_text(self.notebookPagePanelMethodOverview.preambleStart + 'g = mecode.G(direct_write=True, direct_write_mode="serial", printer_port="' + printerPort + '", baudrate=115200)' + self.notebookPagePanelMethodOverview.preambleRest + self.notebookPagePanelMethodOverview.pyotronextTextbox.GetValue() + "\n" + "################################################################ \n" + "time.sleep(12) \n" + "##################################################### \n" + "print('end of method script')" + "\n")  # concatenate strings from the G-code preamble + the editable G-code in the text box of panel "PanelMethodOverview", open the previously specified file and write to disk in the specified file.
            onExportMecodeToFileBtnDialog2.Destroy
        onExportMecodeToFileBtnConfigDialog.Destroy


def main():
    """Definition of main(). This will be called when the application ggcgen.py 
    is run."""
        
    """Create the main frame, add the GUI-elements and show it as long as
    the application ggcgen.py is running."""

    GGCGenApp = wx.App()
    GGCGenApp.locale = wx.Locale(wx.LANGUAGE_DEFAULT)
    GGCGenMainWindowFrame = GGCGenWindowFrame(parent = None, title = "ggcgen")
    GGCGenMainWindowFrame.Show()
    GGCGenApp.MainLoop()  

if __name__ ==  "__main__":  # True when the application is running
    main()

