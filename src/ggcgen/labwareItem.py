"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/labware.py :
Copyright 2015 - 2020 Opentrons Labworks, Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/plate.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/labware.py :
Copyright 2015 - 2020 Opentrons Labworks, Inc.
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains the classes Vial and Plate.  
Language: Python 3.7
"""

import functools  # provides partial (), a more readable and more efficient alternative to using a wrapper function or a lambda expression (https://stackoverflow.com/questions/173687/is-it-possible-to-pass-arguments-into-event-bindings ) for passing additional arguments.to a function (e.g. an event handler method) from another function
import string  # provides the array string.ascii_uppercase for iterating over the ascii alphanumeric characters
import json
import pathlib

import wx  # Python wrapper for wxWidgets (https://pypi.org/project/wxPython/ ). provides classes and methods for GUI development such as wx.EventHandler and the classes that inherit from it (wx.Window, wx.Notebook, wx.Frame, wx.Dialog, wx.Panel), classes for controls such as wx.Controls and wx.Button and event objects such as wx.CommandEvent which emits events such as EVT_BUTTON.
import wx.grid  # provides wx.grid.Grid for grids consisting of cells.

import utils
import configurator


class Vial:
    config = configurator.Config
    vialDistances = 1 
    xVialCount = 1
    yVialCount = 1
    xFrameSize = 1
    yFrameSize = 1
    bottomHight = 1
    plateHight = 3
    def __init__(self, parent, plateHight, bottomHight, col, row, rowLabel, colLabel, xPos, yPos):
        super().__init__()   # implicit inheritance using super is superior to explicit inheritance
        self.prnt = parent
        self.plateHight = plateHight
        self.bottomHight = bottomHight
        self.col = col
        self.row =row
        self.rowLabel = rowLabel
        self.colLabel = colLabel
        self.label = rowLabel + colLabel
        self.xPos = xPos
        self.yPos = yPos
    def top(self, z=0):
        """
        :param z: z offset in mm
        :return: a Point corresponding to the absolute position of the
                 top-center of the well relative to the deck (with the
                 front-left corner of slot 1 as (0,0,0)). If z is specified,
                 returns a point offset by z mm from top-center
        """
        position = (self.xPos, self.yPos, self.plateHight + z)
        return position

    def bottom(self, z=0):
        """
        :param z: the z distance in mm
        :return: a tupel (x,y,z)
        """
        position = (self.xPos, self.yPos, self.bottomHight + z)
        return position

    def center(self):
        """
        :return: a tupel (x,y,z)
        """
        position = (self.xPos, self.yPos, (self.plateHight-self.bottomHight)/2)
        return position

        
class LabwareItem:  # Python 3 only has new style classes that inherit from object by default
    """
    All labgear items inherit form this class.
    @param: ID: unsigned integer, unique identifier
    @param: (x/y)Slot: unsigned integer, slot of the grid defined by the arrays config.(x/y)SlotsBLoffsets that is occupied by a specific item 
    @param: (x/y)VialDistances: distance between vial centers in mm
    @param: (x/y)VialCount: unsigned integer
    @param: (x/y)FrameSize: unsigned integer, distance between rims of the plate and walls of the top left well
    @param: vialRadius: unsigned integer, distance between vessel center and wall of that vessel in mm
    @param: bottomHight: unsigned integer, distance between surface of the robot's mobile ground plate and the surface of the vessel bottom in mm 
    @param: plateHight: unsigned integer, distance between surface of the robot's mobile ground plate and the top edge of the item / plate in mm
    """ 
    config = configurator.Config.config
    xVialDistances = 1 
    yVialDistances = 1 
    xVialCount = 1
    yVialCount = 1
    xFrameSize = 1
    yFrameSize = 1
    bottomHight = 1
    plateHight = 3
    def __init__(self, definition=None, IDnum=1, slot=0):
        super().__init__()   # implicit inheritance using super is superior to explicit inheritance
        self.IDnum = IDnum
        self.slot=slot
        self.xSlot = LabwareItem.config["slots"][slot][0]
        self.ySlot = LabwareItem.config["slots"][slot][1]
        self.definition=definition
        self.bottomHight = self.definition["wells"]["A1"]["z"]
        self.plateHight = self.definition["wells"]["A1"]["depth"] + self.definition["wells"]["A1"]["z"]
        self.name = self.definition["parameters"]["loadName"] + "_" + str(self.IDnum)
        self.origVialLabels = {key: key for key in self.definition["wells"].keys()}
        self.xVialCount = len(self.definition["ordering"])
        self.yVialCount = len(self.definition["ordering"][0])
        self.vialDiameter = self.definition["wells"]["A1"]["diameter"]
        self.rowLabels = [string.ascii_uppercase[rowIdx] for rowIdx in range(0, self.yVialCount)]
        self.colLabels = [str(colIdx) for colIdx in range(1, self.xVialCount+1)]
        self.xVialPos = {self.origVialLabels[well] : (self.config["xSlotBLoffsets"][self.xSlot] + self.definition["cornerOffsetFromSlot"]["x"] + self.definition["wells"][well]["x"]) for well in self.origVialLabels}
        self.yVialPos = {self.origVialLabels[well] : (self.config["ySlotBLoffsets"][self.ySlot] + self.definition["cornerOffsetFromSlot"]["y"] + self.definition["wells"][well]["y"]) for well in self.origVialLabels}
        self.vialPos = {self.origVialLabels[well] : (self.xVialPos[well], self.yVialPos[well]) for well in self.origVialLabels}
        self.xColPos = [(self.config["xSlotBLoffsets"][self.xSlot] + self.definition["cornerOffsetFromSlot"]["x"] + self.definition["wells"]["A" + well]["x"]) for well in self.colLabels]
        self.yRowPos = [(self.config["ySlotBLoffsets"][self.ySlot] + self.definition["cornerOffsetFromSlot"]["y"] + self.definition["wells"][well + "1"]["y"]) for well in self.rowLabels]
        if self.xVialCount == 1:
            self.xVialDistances = self.vialDiameter
        else:
            self.xVialDistances=self.xVialPos["A2"]-self.xVialPos["A1"]
        if self.xVialCount == 1:
            self.yVialDistances = self.vialDiameter
        else:
            self.yVialDistances=self.yVialPos["A1"]-self.yVialPos["B1"]

        self.pltName=self.definition["parameters"]["loadName"] + "_" + str(self.IDnum)
        self.vialLabels = [[(self.rowLabels[rIdx] + self.colLabels[cIdx]) for cIdx in range(0, self.xVialCount)] for rIdx in range(0, self.yVialCount)]
        self.vialLabelList = []
        list(map(self.vialLabelList.extend, self.vialLabels))
        self.format = self.definition["parameters"]["format"]
        self.vialInstances = { (self.rowLabels[rowIdx] + self.colLabels[colIdx]) : Vial(parent=self, plateHight = self.plateHight, bottomHight = self.bottomHight, col=colIdx, row=rowIdx, rowLabel=self.rowLabels[rowIdx], colLabel=self.colLabels[colIdx], xPos = self.xColPos[colIdx], yPos = self.yRowPos[rowIdx]) for rowIdx in range(0, self.yVialCount) for colIdx in range(0, self.xVialCount) }
        
        self.screenSize = ((int(self.definition["cornerOffsetFromSlot"]["x"] + self.definition["dimensions"]["xDimension"]) * 2), int((self.definition["cornerOffsetFromSlot"]["y"] + self.definition["dimensions"]["yDimension"]) * 2))
        self.topLeftScreenPos = ((int(self.definition["cornerOffsetFromSlot"]["x"] + self.config["xSlotBLoffsets"][self.xSlot] * 2)), (int((self.config["ySizeWorkingSpace"] - self.config["ySlotBLoffsets"][self.ySlot] - self.definition["cornerOffsetFromSlot"]["y"] - self.definition["wells"]["A1"]["y"]) * 2)))
        self.yScreenPosFirstVial = int((self.definition["dimensions"]["yDimension"]-self.definition["wells"]["A1"]["y"]) * 2 - self.definition["wells"]["A1"]["diameter"])
        self.xScreenPosFirstVial = int((self.definition["wells"]["A1"]["x"] * 2) - self.definition["wells"]["A1"]["diameter"])
        self.topLeftVialPos = (self.xScreenPosFirstVial , self.yScreenPosFirstVial)
        self.tipLength = 0
        if "tipLength" in self.definition["parameters"]:
            self.tipLength = self.definition["parameters"]["tipLength"]
        if self.definition["parameters"]["loadName"] == "geb_96_tiprack_10ul":
            self.tipLength = 30
        if self.definition["parameters"]["loadName"] == "opentrons_96_tiprack_10ul":
            self.tipLength = 30
        if self.definition["parameters"]["loadName"] == "opentrons_96_tiprack_20ul":
            self.tipLength = 60
        if self.definition["parameters"]["loadName"] == "tipone_96_tiprack_200ul":
            self.tipLength = 70
        if self.definition["parameters"]["loadName"] == "opentrons_96_tiprack_300ul":
            self.tipLength = 75
        if self.definition["parameters"]["loadName"] == "opentrons_96_tiprack_10ul":
            self.tipLength = 90
        if self.definition["parameters"]["loadName"] == "geb_96_tiprack_1000ul":
            self.tipLength = 90
    
    def wells_by_name(self):
        return self.vialInstances

    def getLabwareDefinition(self, loadName):
        definitionFileName = f"{loadName}.json" 
        definitionFilePath = utils.getResourcePath("labware", definitionFileName)
        try: 
            with definitionFilePath.open() as labwareDefinition:
                labwareDefinition = json.load(definitionFilePath)
        except OSError:
            wx.LogError("Cannot open file")
        return labwareDefinition
    
    def getVialPosX(self, well=""):
        vialPosX = self.config["xSlotBLoffsets"][self.xSlot] + self.definition["cornerOffsetFromSlot"]["x"] + self.definition["wells"][well]["x"]
        return vialPosX
      
    def getVialPosY(self, well=""):
        vialPosY = self.config["ySlotBLoffsets"][self.ySlot] + self.definition["cornerOffsetFromSlot"]["y"] + self.definition["wells"][well]["y"]
        return vialPosY
    
    def getRowLabel(self, rowIdx):
        return string.ascii_uppercase[rowIdx]
       
    def getColLabel(self, colIdx):
        return str(colIdx + 1)    

    def wells(self):
        wellList = []
        for label in self.vialLabelList:
            wellList.append(self.vialInstances[label])
        return wellList
        
    def columns(self):
        cols = [[self.vialLabels[(self.rowLabels[rIdx] + self.colLabels[cIdx])] for cIdx in range(0, self.xVialCount)] for rIdx in range(0, self.yVialCount)]
        return cols
        
    def rows(self):
        rows = [[self.vialLabels[(self.rowLabels[rIdx] + self.colLabels[cIdx])] for rIdx in range(0, self.yVialCount)] for cIdx in range(0, self.cVialCount)]
        return rows

    def columns_by_name(self):
        cols = {self.colLabels()[cIdx]: self.columns()[cIdx] for cIdx in range(0, self.xVialCount)}
        return cols
        
    def rows_by_name(self):
        rows = {self.rowLabels()[rIdx]: self.rows()[rIdx] for rIdx in range(0, self.yVialCount)}
        return rows

    def __getitem__(self, key = ""):
        return self.vialinstances[key]
        
    
