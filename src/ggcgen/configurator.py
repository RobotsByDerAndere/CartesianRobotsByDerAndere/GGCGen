"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/configurator.py
@authors 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of ggcgen. 
@details ggcgen is a Graphical G-code generator and robot control software. 
This file contains the class Config that contains the parameters from the 
config.INI file.  
Language: Python 3.7
"""

import pathlib

import configobj

import utils

class Config:
    currentScriptDirectory = utils.getParentDirectory()
    configFileName = "config.ini"
    configFilePath = currentScriptDirectory / configFileName
    with configFilePath.open() as configFileHandler:
        config = configobj.ConfigObj(infile=configFileHandler, unrepr=True)
