"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/robot.py
@authors 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/ggcgen/blob/master/AUTHORS)
@copyright  2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of ggcgen. 
@details ggcgen is a Graphical G-code generator and robot control software. 
This file contains the utility functions getParentDirectory(), getResourcePath() and getResourcePathString(). This file and a directory with name "resources" must be located in the same directory as the file calling these functions to get the parent directory
Language: Python 3.7
"""

import os  # provides fsdecode()
import pathlib  # provides Path()
import inspect  # provides getsourcefile()

def getParentDirectory():
    """
    Get the parent directory of this file.
    """
    currentScriptDirectory = pathlib.Path(inspect.getsourcefile(lambda:0)).resolve().parent
    return currentScriptDirectory

def getResourcePath(subdirectory, resourceName):
    resourcesDirectory = getParentDirectory()
    subDirectoryPath = resourcesDirectory / subdirectory
    resourcePath = subDirectoryPath / resourceName
    return resourcePath

def getResourcePathString(resourceName):
    resourcePath = getResourcePath("resources", resourceName)
    resourcePathStr = os.fsdecode(resourcePath)
    return resourcePathStr
