"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/plate.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright  2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains the classes Vial and Plate.  
Language: Python 3.7
"""

import pathlib
import functools  # provides partial(), a more readable and more efficient alternative to using a wrapper function or a lambda expression (https://stackoverflow.com/questions/173687/is-it-possible-to-pass-arguments-into-event-bindings ) for passing additional arguments.to a function (e.g. an event handler method) from another function
import string  # provides the array string.ascii_uppercase for iterating over the ascii alphanumeric characters
import json

import wx  # Python wrapper for wxWidgets (https://pypi.org/project/wxPython/ ). provides classes and methods for GUI development such as wx.EventHandler and the classes that inherit from it (wx.Window, wx.Notebook, wx.Frame, wx.Dialog, wx.Panel), classes for controls such as wx.Controls and wx.Button and event objects such as wx.CommandEvent which emits events such as EVT_BUTTON.
import wx.grid  # provides wx.grid.Grid for grids consisting of cells.     

import utils
import configurator
import labwareItem
import robot

robot.VirtualRobot.currentZhight

class LabwareItem:  # Python 3 only has new style classes that inherit from object by default
    """
    All labgear items inherit form this class.
    @param: ID: unsigned integer, unique identifier
    @param: (x/y)Slot: unsigned integer, slot of the grid defined by the arrays config.(x/y)SlotsBLoffsets that is occupied by a specific item 
    @param: (x/y)VialDistances: distance between vial centers in mm
    @param: (x/y)VialCount: unsigned integer
    @param: (x/y)FrameSize: unsigned integer, distance between rims of the plate and walls of the top left well
    @param: vialRadius: unsigned integer, distance between vessel center and wall of that vessel in mm
    @param: bottomHight: unsigned integer, distance between surface of the robot's mobile ground plate and the surface of the vessel bottom in mm 
    @param: plateHight: unsigned integer, distance between surface of the robot's mobile ground plate and the top edge of the item / plate in mm
    """ 
    config = configurator.Config.config
    xVialDistances = 1, 
    yVialDistances = 1, 
    xVialCount = 1, 
    yVialCount = 1,
    xFrameSize = 1,
    yFrameSize = 1,
    bottomHight = 1,
    plateHight = 3
    def __init__(self, definition=None, IDnum=1, slot=0):
        super().__init__()   # implicit inheritance using super is superior to explicit inheritance
        self.IDnum = IDnum
        self.slot=slot
        self.xSlot = LabwareItem.config["slots"][slot][0]
        self.ySlot = LabwareItem.config["slots"][slot][1]
        self.definition=definition
        self.bottomHight = self.definition["wells"]["A1"]["z"]
        self.plateHight = self.definition["wells"]["A1"]["depth"] + self.definition["wells"]["A1"]["z"]
        self.name = self.definition["parameters"]["loadName"] + "_" + str(self.IDnum)
        self.origVialLabels = {key: key for key in self.definition["wells"].keys()}
        self.xVialCount = len(self.definition["ordering"])
        self.yVialCount = len(self.definition["ordering"][0])
        self.vialDiameter = self.definition["wells"]["A1"]["diameter"]
        self.rowLabels = [string.ascii_uppercase[rowIdx] for rowIdx in range(0, self.yVialCount)]
        self.colLabels = [str(colIdx) for colIdx in range(1, self.xVialCount+1)]
        self.xVialPos = {self.origVialLabels[well] : (self.config["xSlotBLoffsets"][self.xSlot] + self.definition["cornerOffsetFromSlot"]["x"] + self.definition["wells"][well]["x"]) for well in self.origVialLabels}
        self.yVialPos = {self.origVialLabels[well] : (self.config["ySlotBLoffsets"][self.ySlot] + self.definition["cornerOffsetFromSlot"]["y"] + self.definition["wells"][well]["y"]) for well in self.origVialLabels}
        self.vialPos = {self.origVialLabels[well] : (self.xVialPos[well], self.yVialPos[well]) for well in self.origVialLabels}
        self.xColPos = [(self.config["xSlotBLoffsets"][self.xSlot] + self.definition["cornerOffsetFromSlot"]["x"] + self.definition["wells"]["A" + well]["x"]) for well in self.colLabels]
        self.yRowPos = [(self.config["ySlotBLoffsets"][self.ySlot] + self.definition["cornerOffsetFromSlot"]["y"] + self.definition["wells"][well + "1"]["y"]) for well in self.rowLabels]
        self.xVialDistances=self.xVialPos["A2"]-self.xVialPos["A1"]
        self.yVialDistances=self.yVialPos["A1"]-self.yVialPos["B1"]
        self.pltName=self.definition["parameters"]["loadName"] + "_" + str(self.IDnum)
        self.vialLabels = [[(self.rowLabels[rIdx] + self.colLabels[cIdx]) for cIdx in range(0, self.xVialCount)] for rIdx in range(0, self.yVialCount)]
        self.vialInstances = { (self.rowLabels[rowIdx] + self.colLabels[colIdx]) : labwareItem.Vial(parent=self, plateHight = self.plateHight, bottomHight = self.bottomHight, col=colIdx, row=rowIdx, rowLabel=self.rowLabels[rowIdx], colLabel=self.colLabels[colIdx], xPos = self.xColPos[colIdx], yPos = self.yRowPos[rowIdx]) for rowIdx in range(0, self.yVialCount) for colIdx in range(0, self.xVialCount) }
        
        self.screenSize = ((int(self.definition["cornerOffsetFromSlot"]["x"] + self.definition["dimensions"]["xDimension"]) * 2), int((self.definition["cornerOffsetFromSlot"]["y"] + self.definition["dimensions"]["yDimension"]) * 2))
        self.topLeftScreenPos = ((int(self.definition["cornerOffsetFromSlot"]["x"] + self.config["xSlotBLoffsets"][self.xSlot] * 2)), (int((self.config["ySizeWorkingSpace"] - self.config["ySlotBLoffsets"][self.ySlot] - self.definition["cornerOffsetFromSlot"]["y"] - self.definition["wells"]["A1"]["y"]) * 2)))
        self.yScreenPosFirstVial = int((self.definition["dimensions"]["yDimension"]-self.definition["wells"]["A1"]["y"]) * 2 - self.definition["wells"]["A1"]["diameter"])
        self.xScreenPosFirstVial = int((self.definition["wells"]["A1"]["x"] * 2) - self.definition["wells"]["A1"]["diameter"])
        self.topLeftVialPos = (self.xScreenPosFirstVial , self.yScreenPosFirstVial)
        

        
    def getLabwareDefinition(self, loadName):
        currentScriptDirectory = utils.getParentDirectory()
        definitionFileName = f"{loadName}.json" 
        definitionFilePath = utils.getResourcePath("labware", definitionFileName)
        labwareDefinition = json.load(definitionFilePath)
        return labwareDefinition
    
    def getVialPosX(self, well=""):
        vialPosX = self.config["xSlotBLoffsets"][self.xSlot] + self.definition["cornerOffsetFromSlot"]["x"] + self.definition["wells"][well]["x"]
        return vialPosX
      
    def getVialPosY(self, well=""):
        vialPosY = self.config["ySlotBLoffsets"][self.ySlot] + self.definition["cornerOffsetFromSlot"]["y"] + self.definition["wells"][well]["y"]
        return vialPosY
    
    def getRowLabel(self, rowIdx):
        return string.ascii_uppercase[rowIdx]
       
    def getColLabel(self, colIdx):
        return str(colIdx + 1)    

    def wells(self, label):
        return self.vialinstances[label]
        
    def __getitem__(self, key = ""):
        return self.vialinstances[key]
        
    
class PanelPlate(wx.Panel):

    """
    All labgear items inherit form this class which in turn inherits from plate.LabwareItem.
    @param: ID: unsigned integer, unique identifier
    @param: (x/y)Slot: unsigned integer, slot of the grid defined by the arrays config.(x/y)SlotsBLoffsets that is occupied by a specific item 
    @param: (x/y)VialDistances: distance between vial centers in mm
    @param: (x/y)VialCount: unsigned integer
    @param: (x/y)FrameSize: unsigned integer, distance between rims of the plate and walls of the top left well
    @param: vialRadius: unsigned integer, distance between vessel center and wall of that vessel in mm
    @param: bottomHight: unsigned integer, distance between surface of the robot's mobile ground plate and the surface of the vessel bottom in mm 
    @param: plateHight: unsigned integer, distance between surface of the robot's mobile ground plate and the top edge of the item / plate in mm
    @param: pltGrid: wx.grid.Grid for selecting single or multiple cells representing 
            vessels. Simple single selections using left mouse button are handled 
            directly and result in sending of a method block containing mecode move commands 
            to MethodOverviewPanel.pyotronextTextbox. If the current z-position is lower 
            than the target z-hight, the z-position is adjusted first.
    @param: pltGetSelBtn: wx.Button for confirming selection of multiple cells of pltGrid. 
            A method block containing move commands is sent to MethodOverviewPanelpyotronextTextbox.
            If the current z-position is lower than the target z-hight, the z-position 
            is adjusted first.
    """ 
    xVialDistances = 1 
    yVialDistances = 1 
    xVialCount = 1 
    yVialCount = 1
    xFrameSize = 1
    yFrameSize = 1
    bottomHight = 1
    plateHight = 3
    
    def __init__(self, 
                 parent=None, 
                 pos=(0,0),
                 size=(0,0), 
                 definition=None, 
                 IDnum=0, 
                 slot=0, 
                 ):
        super().__init__(parent=parent, pos=pos, size=size)   # implicit inheritance using super is superior to explicit inheritance
        self.attributes = labwareItem.LabwareItem(definition=definition, 
                                                  IDnum = IDnum, 
                                                  slot = slot, 
                                                  )
        self.prnt = parent
        self.topLeftVialPos = self.attributes.topLeftVialPos
        
        self.pltGrid = wx.grid.Grid(parent=self, 
                                    pos=(self.topLeftVialPos[0], self.topLeftVialPos[1]), 
                                    size=(size[0]-self.attributes.xScreenPosFirstVial -5, size[1]-self.attributes.yScreenPosFirstVial  - 5))
        self.pltGrid.DisableDragGridSize()
        self.pltGrid.EnableDragCell(enable=False)
        self.pltGrid.EnableEditing(edit=False)  # make cell contents uneditable labels
        self.pltGrid.SetColLabelSize(height=0)
        self.pltGrid.SetRowLabelSize(width=0)
        self.pltGrid.CreateGrid(numRows=self.attributes.yVialCount, numCols=self.attributes.xVialCount, selmode=wx.grid.Grid.SelectCells)        
        for rowIndex in range(0, self.attributes.yVialCount):
            for colIndex in range(0, self.attributes.xVialCount):
                self.pltGrid.SetCellFont(row=rowIndex, col=colIndex, font=wx.Font(wx.FontInfo(pixelSize=(3,6)).Family(wx.FONTFAMILY_DEFAULT)))
                self.pltGrid.SetCellValue(row=rowIndex, col=colIndex, s=(self.attributes.rowLabels[rowIndex]) + (self.attributes.colLabels[colIndex]))
                self.pltGrid.SetColSize(col=colIndex, width=int(self.attributes.xVialDistances * 2))
                self.pltGrid.SetRowSize(row=rowIndex, height=int(self.attributes.yVialDistances * 2))
                self.pltGrid.SetCellAlignment(rowIndex, colIndex, wx.ALIGN_CENTER, wx.ALIGN_CENTER)
        self.pltGrid.ClipHorzGridLines(clip=True)  # Limit grid lines to specified grid size
        self.pltGrid.ClipVertGridLines(clip=True) 
        self.pltGetSelBtn = wx.Button(parent=self, label="Get selection", pos=(10, 1), size=(-1, 6))  
        self.pltGetSelBtn.Bind(event=wx.EVT_BUTTON, handler=functools.partial(self.onGetSelBtn, plate=self, grid=self.pltGrid))  # use functools.partial instead of lambda to pass additional arguments besides the handler function. This is needed to pass information on the event's origin.
        self.pltGetSelBtn.SetFont(wx.Font(wx.FontInfo(pixelSize=(3,5)).Family(wx.FONTFAMILY_DEFAULT)))    
        self.pltGrid.Bind(event=wx.grid.EVT_GRID_SELECT_CELL, handler=functools.partial(self.onSingleSelectionMoveXY, plate=self, grid = self.pltGrid))  # use functools.partial instead of lambda to pass additional arguments besides the handler function. This is needed to pass information on the event's origin.
        self.selPos = None 
#        self.pltGrid.ForceRefresh()

                       
    def getScreenPosX(self, colIdx):
        return (self.attributes.xFrameSize + self.attributes.config["xSlotBLoffsets"][self.xSlot] - self.attributes.vialRadius + self.attributes.xVialDistances * colIdx) * 2
    
    def getScreenPosY(self, rowIdx):
        return (self.attributes.config["ySizeWorkingSpace"] - self.attributes.config["ySlotBLoffsets"][self.ySlot] - self.attributes.yFrameSize + self.attributes.vialRadius + self.attributes.yVialDistances * (rowIdx - self.attributes.yVialCount)) * 2
    

    def onSingleSelectionMoveXY(self, cellSelectEvent, plate, grid):
        """
        Upon simple single selection of a cell representing a vessel using left mouse 
        button click, a method block containing mecode move commands is sent to 
        MethodOverviewPanelpyotronextTextbox. If the current z-position is lower than 
        the target z-hight, the z-position is adjusted first. If Control key was 
        pressed during the selection, the event is skipped to handle multi-selections
        """
        if wx.GetKeyState(wx.WXK_CONTROL) == True:
            cellSelectEvent.Skip()            
        else:
            newZhight = robot.VirtualRobot.zOffset() + plate.plateHight + 5
            col = cellSelectEvent.GetCol() 
            row = cellSelectEvent.GetRow()
            xPos = plate.attributes.xColPos[col]
            yPos = plate.attributes.yRowPos[row]
            cellSelectEvent.Skip()
            commandlist = ["",
                           plate.attributes.config["instrumentName"] + ".move_to(location=" + str(plate.attributes.name) + "['" + plate.attributes.rowLabels[row] + plate.attributes.colLabels[col] + "'])",
                           "##################################################"
            ]
                
            command = "\n    ".join(commandlist)
                
            self.GetTopLevelParent().notebookInitUI.GetPage(1).pyotronextTextbox.WriteText(text=command)
            robot.VirtualRobot.currentZhight = newZhight


    def onGetSelBtn(self, btnEvent, plate, grid):
        """
        Upon left mouse button click on the GetSelBtn for a plate, a method block
        is sent to MethodOverviewPanelpyotronextTextbox which contains a for loop 
        for iteration through all selected cells representing vessels. selections 
        of multiple cells using (ctrl + left mouse button clicks) have to be treated
        seperately from block selections resulting from (ctrl + left mouse button drag)
        If the current z-position is lower than the target z-hight, the z-position 
        is adjusted first.
        """
        
        cells=[]
        selVials = []
        individualMultiselectedCells = grid.GetSelectedCells()  # GridCellCoordsArray for individual cells selected during multi-selection with (Ctrl + LMB) 
        if individualMultiselectedCells: 
            cells.extend([(selCell[0], selCell[1]) for selCell in individualMultiselectedCells])  # translate GridCellCoordsArray to list of tuples with cell indices using list comprehension.
            selVials.extend([(plate.attributes.rowLabels[selCell[1]] + plate.attributes.colLabels[selCell[0]]) for selCell in cells])  # translate cell indices to positions
        if grid.GetSelectionBlockTopLeft():  # block selections have to be added to the single selections
            top_lefts = grid.GetSelectionBlockTopLeft()
            bottom_rights = grid.GetSelectionBlockBottomRight()  # now we know start and endpoint for each block
            blockCount = len(top_lefts)
            rows_starts = [block[0] for block in top_lefts]
            rows_ends = [block[0] for block in bottom_rights]
 
            cols_starts = [block[1] for block in top_lefts]
            cols_ends = [block[1] for block in bottom_rights]
            blockColLimits = list(zip(cols_starts, cols_ends))  # reorganize lists. zip object has to be converted to list
            blockRowLimits = list(zip(rows_starts, rows_ends))
    
            rowsBlocks = [list(range(blockStarts, blockEnds+1)) for blockStarts, blockEnds in blockRowLimits]  # get new list using list comprehension with range to convert limits to list of all selected cell rows 
            colsBlocks = [list(range(blockStarts, blockEnds+1)) for blockStarts, blockEnds in blockColLimits]  # same as above for columns
            for block in range(blockCount):
                cells.extend([(row, col) for row in rowsBlocks[block] for col in colsBlocks[block]])  # add block selections to single selected cells
            
#            selPositions.extend([(plate.attributes.xVialPos[selCell[1]], plate.attributes.yVialPos[selCell[0]]) for selCell in cells])  # convert cell indices to positions
            selVials.extend([(plate.attributes.rowLabels[selCell[0]] + plate.attributes.colLabels[selCell[1]]) for selCell in cells])

        newZhight = robot.VirtualRobot.zOffset() + plate.attributes.plateHight + 5
            
        commandlist = ["",
                       "# iterate through selected positions of " + str(plate.attributes.pltName) + " :",
                       "selectedPlate = " + str(plate.attributes.pltName),
                       "selectedWells = " + str(selVials),
                       "for well in selectedWells:",
                       "    " + plate.attributes.config["instrumentName"] + ".move_to(location=" + plate.attributes.pltName + "[well])", 
                       "###################################################"
                       ]
        command = "\n    ".join(commandlist)
#        mecodeText = "\n" + "# iterate through selected positions of " + str(plate.attributes.pltName) + " : \n" + "selectedPlateHight = " + str(plate.attributes.plateHight) + " \n" + "positionAbovePlate = " + str(newZhight) + " \n" + "selectedPosX = " + str([selPos[0] for selPos in selPositions]) + " \n" + "selectedPosY = " + str([selPos[1] for selPos in selPositions]) + " \n" + "for selX, selY in zip(selectedPosX, selectedPosY): \n" + "    g.abs_move(z=positionAbovePlate, F=" + str(self.attributes.config["zSpeeds"][2]) + ")  # move to z plane above target \n" + "    g.abs_move(x=selX, y=selY, F=" + str(plate.attributes.config["xySpeeds"][2]) + ") \n" + "################################################### \n"
        self.GetTopLevelParent().notebookInitUI.GetPage(1).pyotronextTextbox.WriteText(text=command)
        robot.VirtualRobot.currentZhight = newZhight
                        
