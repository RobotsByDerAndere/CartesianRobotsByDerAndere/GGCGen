'''
This file is required by the program GGCGen (Copyright (c) 2018-2021 DerAndere and other GGCGen authors, see GGCGen AUTHORS file
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS), licensed under 
the terms of the Apache License, Version 2.0). Deleting this file from the GGCGen program directory results in malfunction of GGCGen
'''
