"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
SPDX-License-Identifier: Apache-2.0
"""


"""
@file ggcgen/dialogs.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains Dialog classes (wxPython Dialogs as GUI elements).  
Language: Python 3.7
"""

import webbrowser  # provides webbrowser.open() for opening URL from hyperlinks given as string

import wx  # Python wrapper for wxWidgets (https://pypi.org/project/wxPython/ ). provides classes and methods for GUI development such as wx.EventHandler and the classes that inherit from it (wx.Window, wx.Notebook, wx.Frame, wx.Dialog, wx.Panel), classes for controls such as wx.Controls and wx.Button and event objects such as wx.CommandEvent which emits events such as EVT_BUTTON.


allCenterBorder5Flags = wx.SizerFlags().Align(wx.ALIGN_CENTER).Border(wx.ALL, 5).Proportion(0)  # wx.SizerFlags are the preferred way for setting sizers

class About_Dialog(wx.Dialog):  # inherits from the wx.Dialog class
    """Dialog GUI element for showing information about the application GGCGen. 
    This dialog is shown if in the main frame of GGCGen 
    the button menu Help -> About was clicked."""
    def __init__(self, **kwargs):  # __init__ method is called first when an instance of this class is instantiated at runtime of the program. wx.Dialog classes are instantiated by specific user input during runtime of the program. In Python, the first argument of methods is the instance the method is called on (similar to the keyword this for member functions in C++). By convention, self is the object name for instance methods as opposed to cls for class methods. **kwargs is replaced by all keyword-arguments given when calling the function. 
        super().__init__(**kwargs)  # calls the __init__ method that is defined in the class wx.Dialog from which the New_Layout_Dialog inherits. This refers to itself (self). parent = None means that the dialog is no child of the main frame,  but opens a new frame instead.   
              
        self.aboutTextHtmlWindow = wx.html.HtmlWindow(parent=self, style=wx.html.HW_SCROLLBAR_AUTO, size=(800, 600))  # selectable/copyable html-formatted text and hyperlinks. See https://github.com/sonicnkt/wpkg-gp-client/blob/master/help.py . As opposed to wx.richtext.RichTextCtrl(style=(wx.richtext.RE_MULTILINE | wx.richtext.RE_READONLY)) emitting a wx.TextUrlEvent, this allows preformatting using the HTML-commands <pre> ... </pre>.
        self.aboutHTMLbody = """
<pre><h1>General Information</h1>
<i>
Title: GGCGen
Authors: DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
Created: 2018 - 2022
Version: 1.0.6
Language: Python 3.7
About: Graphical G-code generator and robot control software. Create and execute 
methods for the 3D-printer-based DIY liquid handling robot PipetBot-A8. 
You can semi-automatically generate protocols that are partially compatible with 
opentrons API version 2 and G-code scripts using a graphical user interface.
Homepage: https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/
Credits: 
DerAndere: Founder, initial design and implementation, 
           core developer, current maintainer
Other GGCGen authors, see GGCGen AUTHORS file
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
: See GGCGen's CHANGELOG file
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/CHANGELOG)
Opentrons Labworks, Inc.: labware definitions and development of the opentrons APIv2
</i>

<h1>NOTICE</h1>

GGCGen (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
See GGCGen AUTHORS file (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)




<h1>Licensing Information</h1>

GGCGen (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
See GGCGen AUTHORS file (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)

This work is licensed under the terms of the Apache License, Version 2.0. For details, see the *.dep5 file and the [./LICENSES/ folder](https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/LICENSES) in the root directory of this work. This work uses the REUSE framework for License compiance.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 



This software includes software developed by DerAndere.

Contributions by DerAndere
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)



This software includes software developed by The GGCGen authors:

Contributions by The GGCGen authors
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS).



This software contains software derived from portions of the Opentrons 
Platform 3.15.2 (parts of opentrons/api/*) by Opentrons Labworks, Inc., with 
various modifications by DerAndere and other GGCGen authors, 
see GGCGen AUTHORS file 
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS):

Opentrons Platform 3.15.2 (https://www.github.com/Opentrons/opentrons)
Copyright 2015 - 2020 Opentrons Labworks, Inc.


<h2>LICENSE</h2>
                                 Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


</pre>
        """
        self.aboutTextHtmlWindow.SetPage(self.aboutHTMLbody)
    
        self.closeBtn = self.CreateButtonSizer(flags=wx.CLOSE)  # creates Cancel button with default action at the bottom
        
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.mainVboxSizer.Add(self.aboutTextHtmlWindow, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.closeBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
    
        self.aboutTextHtmlWindow.Bind(event=wx.html.EVT_HTML_LINK_CLICKED, handler=self.onLinkClicked, source=self.aboutTextHtmlWindow)


    def onLinkClicked(self, link):
        """extract the Href (URL) from the link.
        If link is an anchor link let the html window scroll to the anchor
        If it is not an anchor link let the default browser open"""
        href = link.GetLinkInfo().GetHref()
        anchor = href[1:]
        if href.startswith('#'):
            self.aboutTextHtmlWindow.ScrollToAnchor(anchor)
        else:
            wx.BeginBusyCursor()
            webbrowser.open(href)
            wx.EndBusyCursor()

class ExportConfigDialog(wx.Dialog):
    """Dialog GUI element for specifying the USB serial port to which G-code 
    commands are to be written."""
    def __init__(self, **kwargs):  # __init__ method is called first when an instance of this class is instantiated at runtime of the program. wx.Dialog classes are instantiated by specific user input during runtime of the program. In Python, the first argument of methods is the instance the method is called on (similar to the keyword this for member functions in C++). By convention, self is the object name for instance methods as opposed to cls for class methods. **kwargs is replaced by all keyword-arguments given when calling the function. 
        super().__init__(**kwargs)  # calls the __init__ method that is defined in the class wx.Dialog from which the New_Layout_Dialog inherits. This refers to itself (self). parent = None means that the dialog is no child of the main frame,  but opens a new frame instead.   
        
        self.inputTextCtrlSerialPortDescription = wx.StaticText(parent=self, label='Enter port for serial communication. \n' + 'Verify that the target device is connected to the specified USB port before running the method script. \n' + 'Under Microsoft Windows, open the device manager -> "Ports (COM & LPT)" and \n' + 'copy the COM port given for the target device, e.g. "COM3" (excluding "").')
        self.inputTextCtrlSerialPort = wx.TextCtrl(parent=self, style=wx.TE_LEFT, value = "")    
        self.okCancelBtn = self.CreateButtonSizer(flags=(wx.OK|wx.CANCEL))  # creates OK and Cancel buttons with default actions at the bottom

        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.mainVboxSizer.Add(self.inputTextCtrlSerialPortDescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.inputTextCtrlSerialPort, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.okCancelBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
        self.inputTextCtrlSerialPort.SetFocus()

class Set_Layout_Dialog(wx.Dialog):  # inherits from the wx.Dialog class
    """Dialog GUI element for creation of new customized working space layouts 
    for the panel 'PanelLayoutMove' that represents the PipetBot-A8 working 
    space. This dialog is shown if in the panel Set_Next_Command_Panel of GGCGen 
    the button 'Set layout open wizard' was clicked."""
    def __init__(self, **kwargs):  # __init__ method is called first when an instance of this class is instantiated at runtime of the program. wx.Dialog classes are instantiated by specific user input during runtime of the program. In Python, the first argument of methods is the instance the method is called on (similar to the keyword this for member functions in C++). By convention, self is the object name for instance methods as opposed to cls for class methods. **kwargs is replaced by all keyword-arguments given when calling the function. 
        super().__init__(**kwargs)  # calls the __init__ method that is defined in the class wx.Dialog from which the New_Layout_Dialog inherits. This refers to itself (self). parent = None means that the dialog is no child of the main frame,  but opens a new frame instead.   
        
        self.Layout = ["0: Standard (default): plate96_1, tipHolder200mL96_1, tube15mlRack5x1_1, tube15mLRack1x5_1, vessel40x40x170_1, waste_1", "1: plate24_1, tipHolder200mL96_1, waste_1", "2: plate96_1, tipHolder200mL96_1,  waste_1", "3: multi_select_plate96_1, tipHolder200mL96_1,  waste_1", ]
        
        
        self.choiceLayoutDescription = wx.StaticText(parent=self, label="choose layout")
    
        self.choiceLayout = wx.Choice(self, choices = self.Layout)
        
        self.okCancelBtn = self.CreateButtonSizer(flags=(wx.OK|wx.CANCEL))  # creates OK and Cancel buttons with default actions at the bottom
        
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.mainVboxSizer.Add(self.choiceLayoutDescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.choiceLayout, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.okCancelBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
    

class Move_xy_Dialog(wx.Dialog):
    """Dialog GUI element for freely choosing any movements of the tool in the 
    xy-plane. A radio box for switching between absolute and relative 
    positioning is provided. This Dialog is shown if in the panel 
    Set_Next_Command_Panel of GGCGen the button 'Move xy open wizard' was 
    clicked."""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    
        self.radioboxAbsoluteRelativeMove = wx.RadioBox(parent=self, label="choose absolute target position or relative travel distance", choices=["Absolute positioning", "Relative positioning"])
        
        self.moveXYdescription = wx.StaticText(parent=self, label="enter xy-coordinates in mm. For example X10 Y20.")
        self.inputTextboxXY = wx.TextCtrl(parent=self, style=wx.TE_LEFT, value = "")
        self.inputTextboxXY.SetInitialSize(size=wx.Size(80, -1))
        self.speedDescription = wx.StaticText(parent=self, label="enter speed in mm/min as integer.")
        self.inputTextboxSpeed = wx.TextCtrl(parent=self, style=wx.TE_LEFT, value = "5000")
        self.inputTextboxSpeed.SetInitialSize(size=wx.Size(80, -1))
        
        self.okCancelBtn = self.CreateButtonSizer(flags=(wx.OK | wx.CANCEL))
        
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.hBoxSizer1 = wx.BoxSizer(orient=wx.HORIZONTAL)

        
        self.hBoxSizer1.Add(self.radioboxAbsoluteRelativeMove, proportion=0, flag=wx.ALL, border=5)
        self.mainVboxSizer.Add(self.hBoxSizer1, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.moveXYdescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.inputTextboxXY, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.speedDescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.inputTextboxSpeed, allCenterBorder5Flags)
    
        self.mainVboxSizer.Add(self.okCancelBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
        
        
   
class Dispense_Dialog(wx.Dialog):
    """Dialog GUI element for controlling dispensing (uptake or outlet) of 
    liquids. Volume can be specified and a radio box for toggling on or off 
    wiping (moving the tool along the wall of the vessel to remove remaining 
    traces of liquid from the outside of the pipette tip) is provided.""" 
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.radioboxMode = wx.RadioBox(parent=self, label="choose mode", choices=["contactless", "include wiping"])
    
        self.dispenseDescription = wx.StaticText(parent=self, label="enter volume in microliters as integer (positive integer for uptake)")
        self.inputTextboxDispense = wx.TextCtrl(parent=self, style=wx.TE_LEFT, value = "0.0")
        self.inputTextboxDispense.SetInitialSize(size=wx.Size(80, -1))
        
        self.okCancelBtn = self.CreateButtonSizer(flags=(wx.OK|wx.CANCEL))
        
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.mainVboxSizer.Add(self.radioboxMode, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.dispenseDescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.inputTextboxDispense, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.okCancelBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
    

class Move_Tip_to_Predef_z_Dialog(wx.Dialog):
    """Dialog GUI element for controlling z-move of tip to predefined hights. 
    target plate hight and z-position qualifier can be chosen. Additional offset
    can be specified.""" 
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.listBoxPlateTypeDescription = wx.StaticText(parent=self, label="choose item type to which z-positioning is adjusted")        
        self.itemChoices = ["None", 
                            "plate_96well", 
                            "labware.corning_96_wellplate_360ul_flat",
                            "opentrons_96_tiprack_10ul",
                            "opentrons_96_tiprack_20ul",
                            "opentrons_96_tiprack_300ul",
                            "opentrons_96_tiprack_1000ul",
                            "derandere_1_trash_832ml",
                            "opentrons_1_trash_850ml_fixed"
                            ]
        self.listBoxPlateType = wx.ListBox(parent=self, choices=self.itemChoices, style=wx.LB_SINGLE, name="item type")
        self.zChoices = ["maximal", "above", "center", "low", "bottom"]
        self.radioBoxHight = wx.RadioBox(parent=self, label="choose z hight", choices=self.zChoices, style=wx.RA_SPECIFY_ROWS)
    
        self.zOffsetDescription = wx.StaticText(parent=self, label="enter additional z-offset in millimeters as integer (negative integer for lower z)")
        self.inputTextboxZoffset = wx.TextCtrl(parent=self, style=wx.TE_LEFT, value = "0")
        self.inputTextboxZoffset.SetInitialSize(size=wx.Size(80, -1))
        
        self.okCancelBtn = self.CreateButtonSizer(flags=(wx.OK|wx.CANCEL))
        
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.hBoxSizer1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.subVboxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.subVboxSizer1.Add(self.listBoxPlateTypeDescription, allCenterBorder5Flags)
        self.subVboxSizer1.Add(self.listBoxPlateType, allCenterBorder5Flags)
        
        self.hBoxSizer1.Add(self.subVboxSizer1, allCenterBorder5Flags)        
        self.hBoxSizer1.Add(self.radioBoxHight, allCenterBorder5Flags)
        
        self.mainVboxSizer.Add(self.hBoxSizer1, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.zOffsetDescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.inputTextboxZoffset, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.okCancelBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
    
class AddLabwareDialog(wx.Dialog):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.listBoxSlotDescription = wx.StaticText(parent=self, label="choose slot")        
        self.slotChoices = ["0", "1", "2", "3", "4", "5", "6", "7", "9", "10", "11", "12" ]
        self.listBoxSlot = wx.ListBox(parent=self, choices=self.slotChoices, style=wx.LB_SINGLE, name="slot")
    
        self.okCancelBtn = self.CreateButtonSizer(flags=(wx.OK|wx.CANCEL))
        
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.mainVboxSizer.Add(self.listBoxSlotDescription, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.listBoxSlot, allCenterBorder5Flags)
        self.mainVboxSizer.Add(self.okCancelBtn, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)
    
