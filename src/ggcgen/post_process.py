"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/labware.py :
Copyright 2015 - 2020 Opentrons Labworks, Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/post_process.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright 2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/labware.py :
Copyright 2015 - 2020 Opentrons Labworks, Inc. 
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains Method overview classes (wxPython panel as GUI elements).  
Language: Python 3.7
"""

import pathlib  # provides Path() and Path().write_text(), Path().resolve() and Path().resolve().Parent() for file handling. Replaces os.path and does not need open()
import textwrap  # provides indent() for adding prefixes to lines in strings after each specified newline character. Can be used for indentation.

import wx.html  # provides wx.html.HtmlWindow for selectable/copyable html-preformatted text and hyperlinks. An example is available at https://github.com/sonicnkt/wpkg-gp-client/blob/master/help.py

import pyotronext.execute

import utils
import dialogs
import configurator

allCenterBorder5Flags = wx.SizerFlags().Align(wx.ALIGN_CENTER).Border(wx.ALL, 5).Proportion(0)  # wx.SizerFlags are the preferred way for setting sizers

class PanelMethodOverview(wx.Panel):
    """
    This Panel is shown when selecting the second tab (wx.Notebook page) of GGCGen.
    It contains a non-editable text box containing a fixed preamble and below
    an editable text box where method blocks were sent to from other parts of the
    program. At the bottom, it has buttons for export to a python/pyotronext script, 
    export to gcode (via a temporary python/pyotronext script) and for direct execution
    of the method (via a temporary python/pyotronext script).     
    """
    def __init__(self, **kwargs): 
        super().__init__(**kwargs) 
        self.currentScriptDirectory = utils.getParentDirectory()
        self.tempPyotronextFileName = "tempPyotronext.py"
        self.tempPyotronextFilePath = self.currentScriptDirectory / self.tempPyotronextFileName
        self.instrumentName = configurator.Config.config["instrumentName"]
        self.preambleStart = "''' \n" + "Opentrons APIv2 compatible Python/pyotronext (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/pyotronext) script for controlling lab robots \n" + "that run Marlin 2.0 firmware (http://marlinfw.org/meta/download) \n" + "This script was (partially or as a whole) generated semi-automatically using the open source software GGCGen \n" + "(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen) and pyotronext \n" + "(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/pyotronext). \n" + "GGCGen is Copyright 2018-2022 DerAndere, see GGCGen AUTHORS file (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS), \n" + "and it is licensed under the terms of the Apache License, Version 2.0." + "\n" + "pyotronext is Copyright 2018-2022 DerAndere and other pyotronext authors, see pyotronext AUTHORS file \n" + "(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/pyotronext/blob/master/AUTHORS), \n" + "and it is licensed under the terms of the Apache License, Version 2.0." + "\n" + "''' \n\n" 
        self.preambleImports = "import pyotronext.execute  # see https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master \n" + "from pyotronext import protocol_api  # see https://gitlab.com/RobotsByDerAndere/pyotronext/blob/master" + "\n" + "\n" + "metadata = {'apiLevel': '2.0'}" + "\n" + "\n"
        self.preambleRest = "############################################################################### \n" + "############################################################################### \n"
        self.gCodePreambleHtmlWindow = wx.html.HtmlWindow(parent=self, style=wx.html.HW_SCROLLBAR_NEVER, size=wx.Size(1000,280))  # selectable/copyable html-formatted text and hyperlinks. See https://github.com/sonicnkt/wpkg-gp-client/blob/master/help.py. size has to be specified, otherwise content of wx.htmlHtmlWindow() is hidden 
        self.gCodePreambleHtmlWindow.SetPage("<pre><code>" + self.preambleStart + self.preambleImports + self.preambleRest + "<\code><\pre>")
        self.pyotronextTextbox = wx.TextCtrl(parent=self, style = wx.TE_MULTILINE, size=(1000, 640)) 
        self.pyotronextTextbox.SetFont(font=wx.Font(wx.FontInfo(pointSize=10).Family(wx.FONTFAMILY_MODERN))) 
        
        self.exportProtocolToFileBtn = wx.Button(parent=self, label="Export to Python script")
        self.exportProtocolToFileBtn.Bind(event=wx.EVT_BUTTON, handler=self.onExportPyotronextToFileBtn)
        self.exportGcodeToFileBtn = wx.Button(parent=self, label="Export to G-code file")
        self.exportGcodeToFileBtn.Bind(event=wx.EVT_BUTTON, handler=self.onExportGcodeToFileBtn)
        self.executePyotronextScriptBtn = wx.Button(parent=self, label="Run protocol")
        self.executePyotronextScriptBtn.Bind(event=wx.EVT_BUTTON, handler=self.onExecutePyotronextScriptBtn)
        
        self.hBoxSizer1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.hBoxSizer1.Add(self.exportGcodeToFileBtn, allCenterBorder5Flags)
        self.hBoxSizer1.Add(self.exportProtocolToFileBtn, allCenterBorder5Flags)
        self.hBoxSizer1.Add(self.executePyotronextScriptBtn, allCenterBorder5Flags)

        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.mainVboxSizer.Add(self.gCodePreambleHtmlWindow, proportion=1, flag=(wx.ALL | wx.EXPAND), border=5)
        self.mainVboxSizer.Add(self.pyotronextTextbox, proportion=1, flag=(wx.ALL | wx.EXPAND), border=10)
        self.mainVboxSizer.Add(self.hBoxSizer1, allCenterBorder5Flags)
        
        self.SetSizerAndFit(self.mainVboxSizer)


    def onExportPyotronextToFileBtn(self, exportPyotronextToFileBtnEvent):
        """
        Open wx.FileDialog and save Python/opentrons script as text file with file 
        extension .py. The content is a concatenation of the 
        preamble and the content of the wx.TextCtrl otonextTextbox of the 
        PanelMethodOverview.
        """  
        onExportPyotronextToFileBtnConfigDialog = dialogs.ExportConfigDialog(parent=self, title = "Export Config Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if onExportPyotronextToFileBtnConfigDialog.ShowModal() == wx.ID_OK:
            printerPort = onExportPyotronextToFileBtnConfigDialog.inputTextCtrlSerialPort.GetValue()
            onExportPyotronextToFileBtnDialog2 = wx.FileDialog(parent=self, message="Choose a file", defaultDir=str(pathlib.Path.cwd()), defaultFile="", wildcard="*.py", style=(wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT), name = "Export mecode method as Pythonscript")
            if onExportPyotronextToFileBtnDialog2.ShowModal() == wx.ID_OK:
                filename = onExportPyotronextToFileBtnDialog2.GetFilename()  # save the filename that was actually specified for export in the dialog.
                dirname = pathlib.Path(onExportPyotronextToFileBtnDialog2.GetDirectory())  # looks for the the path to the file that was actually specified for export.
                file = dirname / filename  #creates a path recognizeable by the currently running operating system and opens the previously specified file in mode="w" (open for writing, truncating existing file first. creating the file if it did not exist. 
                exportTextList = [self.preambleStart,
                                  self.preambleImports,
                                  self.preambleRest,
                                  self.pyotronextTextbox.GetValue(),
                                  "",
                                  "    #####################################################",
                                  "    " + self.instrumentName + ".teardown()",
                                  "    #####################################################",
                                  'protocol = pyotronext.execute.get_protocol_api(direct_write=True',
                                  '                                               port="' + printerPort + '", ', 
                                  '                                               baudrate="' + str(configurator.Config.config['baudrate']) + '", ', 
                                  "                                               version='2.0')",
                                  "run(protocol)",
                                  "print('end of method script') ",
                                  "",
                                  ]  # concatenate strings from the G-code preamble + the editable G-code in the text box of panel "PanelMethodOverview" and write to disk in the specified file.
                exportText="\n".join(exportTextList)
                file.write_text(exportText)
            onExportPyotronextToFileBtnDialog2.Destroy
        onExportPyotronextToFileBtnConfigDialog.Destroy
        
        
    def writeAtCurrentPos(self, otronextText):
        self.pyotronextTextbox.WriteText(text=otronextText) 
        
        
    def onExportGcodeToFileBtn(self, generateGcodeBtnEvent):
        """
        Open wx.FileDialog and save Python/mecode script as temporary text 
        file with file extension .py. The content is a concatenation of 1) a 
        preamble that contains the statement to export as a text file with 
        G-code file extension and 2) the content of the wx.TextCtrl gCodeTextbox of the 
        PanelFileGenerator. The temporary Python script is then executed using 
        the Python library subprocess with the now preferred subprocess.run() 
        API. This opens the initially specified target file and writes G-code
        to it.
        """  
        onExportGcodeToFileBtnDialog = wx.FileDialog(parent=self, message="Choose a file", defaultDir=str(pathlib.Path.cwd()), defaultFile="", wildcard="*.gcode", style=(wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT), name = "Export method to G-code script ")
        if onExportGcodeToFileBtnDialog.ShowModal() == wx.ID_OK:
            filename = onExportGcodeToFileBtnDialog.GetFilename()  # save the filename that was actually specified for export in the dialog.
            dirname = pathlib.Path(onExportGcodeToFileBtnDialog.GetDirectory())  # looks for the the path to the file that was actually specified for export.        
            exportTextList = [self.preambleStart,
                              self.preambleImports,
                              self.preambleRest,
                              "",
                              self.pyotronextTextbox.GetValue(),
                              "",
                              "    #####################################################",
                              "    " + self.instrumentName + ".teardown()",
                              "    #####################################################",
                              "",
                              'print("end of method script")', 
                              ]  # concatenate strings from the G-code preamble + the editable protocol in the text box of panel "PanelMethodOverview" and write to disk in the specified file.)        
            exportText = "\n".join(exportTextList)
            print("generate tempPyotronext.py script")
            self.tempPyotronextFilePath.write_text(exportText)
            print("tempPyotronext.py script generated. Start conversion to G-code")
            self.runPythonScript(scriptPath=self.tempPyotronextFilePath, outfile=str(dirname / filename)) #exportReturncode = self.runPythonScript(scriptPath=self.tempPyotronextFilePath)
        onExportGcodeToFileBtnDialog.Destroy
    
    def onExecutePyotronextScriptBtn(self, executePyotronextBtnEvent):
        """
        Open wx.FileDialog and save Python script as temporary text 
        file with file extension .py. The content is a concatenation of 1) a 
        preamble that contains the statement to send G-code commands directly 
        via the USB serial port and 2) the content of the wx.TextCtrl gCodeTextbox of the 
        PanelFileGenerator. The temporary Python script is then executed using 
        the Python library subprocess with the now preferred subprocess.run() 
        API.
        """  
        onExportPyotronextToFileBtnConfigDialog = dialogs.ExportConfigDialog(parent=self, title = "Export Config Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if onExportPyotronextToFileBtnConfigDialog.ShowModal() == wx.ID_OK:
            printerPort = onExportPyotronextToFileBtnConfigDialog.inputTextCtrlSerialPort.GetValue()
            exportTextList = [self.preambleStart,
                              self.preambleImports,
                              self.preambleRest,
                              self.pyotronextTextbox.GetValue(),
                              "",
                              "    #####################################################",
                              "    " + self.instrumentName + ".teardown()",
                              "    #####################################################",
                              "print('end of method script') ",
                              "",
                              ]  # concatenate strings from the G-code preamble + the editable G-code in the text box of panel "PanelMethodOverview" and write to disk in the specified file.
            exportText = "\n".join(exportTextList)
            self.tempPyotronextFilePath.write_text(exportText)
            print("tempPyotronext.py generated. Start execution")
            self.runPythonScript(scriptPath=self.tempPyotronextFilePath, port=printerPort, baudrate=configurator.Config.config['baudrate'])
            self.tempPyotronextFilePath.write_text("'''" + "\n" + "This file is required by the program GGCGen (Copyright (c) 2018-2022 DerAndere and other GGCGen authors, see GGCGen AUTHORS file" + "\n" + "(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS), licensed under \n" + "the terms of the Apache License, Version 2.0). Deleting this file from the GGCGen program directory results in malfunction of GGCGen" + "\n" + "'''" + "\n")
        onExportPyotronextToFileBtnConfigDialog.Destroy           


    def runPythonScript(self, scriptPath, direct_write=False, direct_write_mode="serial", outfile=None, port=configurator.Config.config['port'], baudrate=configurator.Config.config['baudrate']):
        """
        run the Script at the specified path (scriptPath).
        """
        with scriptPath.open() as protocolFilehandler:
            pyotronext.execute.execute(protocol_file=protocolFilehandler, protocol_name="tempPyotronext.py", direct_write=direct_write, direct_write_mode=direct_write_mode, outfile=outfile, port=port, baudrate=baudrate)
        print("pyotronext method script processed")
