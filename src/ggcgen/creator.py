"""
Copyright 2018 - 2022 The GGCGen authors (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) 
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/contexts.py :
Copyright 2015 - 2020 Opentrons Labworks, Inc.
SPDX-License-Identifier: Apache-2.0
"""

"""
@file ggcgen/creator.py
@authors DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues) and other GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
@copyright  2018 - 2022 The GGCGen authors
(https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/blob/master/AUTHORS)
Copyright 2018 - 2022 DerAndere (https://gitlab.com/RobotsByDerAndere/CartesianRobotsByDerAndere/GGCGen/issues)
Based on (with modifications) https://github.com/Opentrons/opentrons/blob/b388c4e8251844f80e1d5f9ab7be338b0e625163/api/src/opentrons/protocol_api/contexts.py :
Copyright 2015 - 2020 Opentrons Labworks, Inc.
License: Apache License, Version 2.0 
(https://www.apache.org/licenses/LICENSE-2.0.txt)
@brief Part of GGCGen. 
@details GGCGen is a Graphical G-code generator and robot control software. 
This file contains the PanelSetNextCommand class (a wxPython panel as a GUI element
for generating strings that are compatible with the Opentrons APIv2).  
Language: Python 3.7
"""

"""
This software is inspired by third-party software. Each third-party software is 
licensed under the terms of its seperate license. The License text 
can be found in the directory 3rd_party_licenses/ . The information in this 
comment block pertains to the respective third-party software. 

opentrons (https://github.com/Opentrons/opentrons)
Copyright (c) 2015-2020 Opentrons Labworks, Inc.
License: Apache License, Version 2.0 
SPDX-License-Identifier: Apache 2.0

"""

import pathlib
import json

import wx  # Python wrapper for wxWidgets (https://pypi.org/project/wxPython/ ). provides classes and methods for GUI development such as wx.EventHandler and the classes that inherit from it (wx.Window, wx.Notebook, wx.Frame, wx.Dialog, wx.Panel), classes for controls such as wx.Controls and wx.Button and event objects such as wx.CommandEvent which emits events such as EVT_BUTTON.
import configobj

import utils
import configurator
import plate
import layout
import dialogs
import robot

currentScriptDirectory = utils.getParentDirectory()

allCenterBorder5Flags = wx.SizerFlags().Align(wx.ALIGN_CENTER).Border(wx.ALL, 5).Proportion(0)  # wx.SizerFlags are the preferred way for setting sizers

       
class PanelSetNextCommand(wx.Panel): 
    """PanelSetNextCommand is the default page of GGCGen. It contains the 
    layout panel, buttons to open the previously defined dialogs ('wizards' for 
    different commands) as well as text-control-panels for direct input as well 
    as a list-box for frequently used standard commands and special commands"""  
    def __init__(self, **kwargs): 
        super().__init__(**kwargs)
        self.nextID=1
        self.config = configurator.Config.config
        """Buttons for opening dialogs/wizards"""
        self.moveXYbtn = wx.Button(parent=self, label = "Move xy \n open wizard")
        self.moveXYbtn.Bind(event=wx.EVT_BUTTON, handler=self.onMoveXYbtn)
        
        self.inputTextboxZ = wx.TextCtrl(parent=self, style=wx.TE_LEFT, value = "140")
        self.inputTextboxZ.SetInitialSize(size=wx.Size(80, -1))
        self.labelTextboxZ = wx.StaticText(parent=self, label='move z \n Enter absolute target z-position in mm \n as integer (e.g. 120) and confirm \n by clicking on button "move z"')

        self.moveZbtn = wx.Button(parent=self, label="move z")
        self.moveZbtn.Bind(event=wx.EVT_BUTTON, handler=self.onMoveZbtn)
        
        self.moveTiptoPredefZbtn = wx.Button(parent=self, label="move tip \n to predefined z \n open wizard")
        self.moveTiptoPredefZbtn.Bind(event=wx.EVT_BUTTON, handler=self.onMoveTiptoPredefZbtn)

        self.dispenseBtn = wx.Button(parent=self, label="Dispense \n open wizard")
        self.dispenseBtn.Bind(event=wx.EVT_BUTTON, handler=self.onDispenseBtn)
        
        self.setLayoutBtn = wx.Button(parent=self, label="Set Layout \n open wizard")
        self.setLayoutBtn.Bind(event=wx.EVT_BUTTON, handler=self.onSetLayoutBtn)

        """ListBox SpecialCommands"""       
        self.specialCommandChoices = ["debug", "initialization", "mix3xY", "pick_up_tip", "drop_tip", "waste_disposal", "empty_pump", "clean3x", "pipetMix3x"]                       
        self.listBoxSpecialCommands = wx.ListBox(parent=self, choices = self.specialCommandChoices, style = wx.LB_SINGLE, name = "Special commands")
        self.listBoxSpecialCommands.Bind(event=wx.EVT_LISTBOX, handler=self.onListBoxSpecialCommandsSelection)

        
        """Add PanelLayoutMove variants and by default hide all but PanelLayout0Move"""
#        self.panelWorkspace = panelLayout.PanelWorkspace(parent=self)
        
        self.panelLayout = layout.PanelLayout(parent=self, layoutFilePath=None)
        self.showLabwarePanels(parent=self.panelLayout)
        self.addLabwareBtn = wx.Button(parent=self, label="Add Labware")
        self.addLabwareBtn.Bind(event=wx.EVT_BUTTON, handler=self.onAddLabwareBtn)

        """Layout using nested sizers"""
        self.mainVboxSizer = wx.BoxSizer(orient=wx.VERTICAL)

        self.hBoxSizer1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.hBoxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.vBoxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)
        self.vBoxSizer2 = wx.BoxSizer(orient=wx.VERTICAL)
        self.vBoxSizer3 = wx.BoxSizer(orient=wx.VERTICAL)
        self.vBoxSizer4 = wx.BoxSizer(orient=wx.VERTICAL)
        self.vBoxSizer5 = wx.BoxSizer(orient=wx.VERTICAL)
        
        self.subHboxSizer1 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.subHBoxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        """Add widgets to the sizers. Add PanelLayoutMove panels to self.hBoxSizer2"""
        self.vBoxSizer1.Add(self.setLayoutBtn, allCenterBorder5Flags)
        self.vBoxSizer1.Add(self.addLabwareBtn, allCenterBorder5Flags)
        self.vBoxSizer1.Add(self.moveXYbtn, allCenterBorder5Flags)
        
        self.vBoxSizer2.Add(self.labelTextboxZ, allCenterBorder5Flags) 
        self.subHboxSizer1.Add(self.inputTextboxZ, proportion=0, flag=(wx.ALL | wx.ALIGN_LEFT), border=5)
        self.subHboxSizer1.Add(self.moveZbtn, proportion=0, flag=wx.ALL, border=5)
 
        self.vBoxSizer2.Add(self.subHboxSizer1, proportion=0, flag=(wx.ALL | wx.ALIGN_CENTER), border=0)
        self.vBoxSizer2.Add(self.moveTiptoPredefZbtn, allCenterBorder5Flags)
        
        self.vBoxSizer3.Add(wx.StaticLine(parent=self, style=wx.LI_VERTICAL), proportion=1, flag=(wx.ALL | wx.EXPAND), border=5)   
        self.vBoxSizer4.Add(self.dispenseBtn, proportion=0, flag=(wx.ALL| wx.ALIGN_CENTER), border=5)
        
        self.vBoxSizer5.Add(self.listBoxSpecialCommands, proportion=0, flag=wx.ALL, border=5)
               
        self.hBoxSizer1.Add(self.vBoxSizer1, proportion=0, flag=(wx.ALL | wx.ALIGN_TOP | wx.ALIGN_CENTER), border=0)
        self.hBoxSizer1.Add(self.vBoxSizer2, proportion=0, flag=(wx.ALL | wx.ALIGN_TOP | wx.ALIGN_CENTER), border=0)
        self.hBoxSizer1.Add(self.vBoxSizer3, proportion=0, flag=(wx.ALL | wx.ALIGN_TOP | wx.ALIGN_CENTER), border=0)
        self.hBoxSizer1.Add(self.vBoxSizer4, proportion=0, flag=(wx.ALL | wx.ALIGN_TOP | wx.ALIGN_CENTER), border=0)
        self.hBoxSizer1.Add(self.vBoxSizer5, proportion=0, flag=(wx.ALL | wx.ALIGN_TOP | wx.ALIGN_CENTER), border=0)
        self.hBoxSizer2.Add(self.panelLayout, proportion=0, flag=(wx.LEFT | wx.RIGHT | wx.ALIGN_CENTER), border = 5)

        self.mainVboxSizer.Add(self.hBoxSizer1, proportion=0, flag=(wx.ALL | wx.ALIGN_CENTER), border=0)
        self.mainVboxSizer.Add(self.hBoxSizer2, proportion=0, flag=(wx.ALL | wx.ALIGN_CENTER), border=0)

        
        self.SetSizerAndFit(self.mainVboxSizer)
            
    
    def showLabwarePanels(self, parent, pyotroNextTextboxPage=0):
        """
        @param pyotroNextTextboxPage: int. Defaut is 0, set 1 if GGCGen is not freshly started
        """
        runCommand = "def run(protocol: protocol_api.ProtocolContext):" + "\n"
        if pyotroNextTextboxPage == 0: 
            self.GetParent().GetPage(pyotroNextTextboxPage).pyotronextTextbox.WriteText(text=runCommand)
        tipRacks = []
        vessels = []
        for key in self.panelLayout.layout.labwareObj.keys():
            definition=self.panelLayout.layout.labwareObj[key].definition
            position=self.panelLayout.layout.labwareObj[key].topLeftScreenPos
            sizeX=self.panelLayout.layout.labwareObj[key].screenSize[0] + 6
            sizeY=self.panelLayout.layout.labwareObj[key].screenSize[1] + 1
            ident = self.panelLayout.layout.labwareObj[key].IDnum
            slot = self.panelLayout.layout.labwareObj[key].slot
            
            self.panelLayout.platePanels.update({key: plate.PanelPlate(parent=parent, pos=(position[0], position[1]-10), size=(sizeX, sizeY), definition=definition, IDnum=ident, slot=slot)})
            self.nextID += 1
            if definition["parameters"]["isTiprack"]:
                tipRacks.append(key)
            commandList = ["",
                           key + ' = protocol.load_labware(load_name="' + definition["parameters"]["loadName"] + '", location=' + str(slot) + ', version=2)',
                          ]
            command = "\n    ".join(commandList)
            self.GetParent().GetPage(pyotroNextTextboxPage).pyotronextTextbox.WriteText(text=command)
        loadInstrumentCommandList = ["",
                                    self.config["instrumentName"] + ' = protocol.load_instrument(instrument_name="' + self.config["instrumentType"] + '", mount="left", tip_racks=[' + ', '.join(tipRacks) + '])',
                                    "#####################################",
                                    ]
        loadInstrumentCommand ="\n    ".join(loadInstrumentCommandList)
        if pyotroNextTextboxPage == 0:
            self.GetParent().GetPage(pyotroNextTextboxPage).pyotronextTextbox.WriteText(text=loadInstrumentCommand)
        
    def onAddLabwareBtn(self, addLabwareBtnEvent):
        with wx.FileDialog(parent=self, name ="Load Labware", 
                wildcard="", 
                style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
                ) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            definitionPathName = fileDialog.GetPath()
            try:
                with open(definitionPathName, 'r') as lDefFileHandler:
                    labwareDefinition = json.load(lDefFileHandler)
            except OSError:
                wx.LogError("Cannot open file")

        onAddLabwareBtnDialog = dialogs.AddLabwareDialog(parent = self, title = "Add Labware Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if onAddLabwareBtnDialog.ShowModal() == wx.ID_OK:
            slot = onAddLabwareBtnDialog.listBoxSlot.GetSelection()
            self.panelLayout.layout.addLabware(definition=labwareDefinition, IDnum=self.nextID, slot=slot)
            
            name = list(self.panelLayout.layout.labwareObj.keys())[-1]
            parent=self.panelLayout
            definition=labwareDefinition
            screenPosition=self.panelLayout.layout.labwareObj[list(self.panelLayout.layout.labwareObj.keys())[-1]].topLeftScreenPos
            screenSizeX = self.panelLayout.layout.labwareObj[list(self.panelLayout.layout.labwareObj.keys())[-1]].screenSize[0] + 6
            screenSizeY = self.panelLayout.layout.labwareObj[list(self.panelLayout.layout.labwareObj.keys())[-1]].screenSize[1] + 1
            ident = self.nextID
            self.nextID += 1
            self.panelLayout.platePanels.update({name: plate.PanelPlate(parent=parent, pos=(screenPosition[0], screenPosition[1]-10), size=(screenSizeX, screenSizeY), definition=definition, IDnum=ident, slot=slot)})
            
        onAddLabwareBtnDialog.Destroy

    
    def onListBoxSpecialCommandsSelection(self, listBoxSpecialCommandsSelectionEvent):
        """On clicking an entry in the the ListBoxSpecialComands, Send method 
        block for that special command. final z-hight is logged. If one of the 
        tipChange commands were selected, increase counters as required to 
        iterate through the tips."""
        selection = self.listBoxSpecialCommands.GetSelection()
        if selection == 0:  # debug
            newZ = self.config["zSizeWorkingSpace"] - 10
            commandList = ["",
                           "# debug.", 
                           "# Attention! remove tip and make sure the workspace is empty", 
                           "# before performing the debug move!", 
                           self.config["instrumentName"] + ".move(z=" + str(self.config["zHights"][4]) + ", F=" + str(self.config["zSpeeds"][1]) + ")  # Retract tool to prevent collision with obstacles",
                           self.config["instrumentName"] + ".move(x=0, y=0, z=" + str(self.config["zHights"][4]) + ", F=" + str(self.config["zSpeeds"][1]) + ")  # Move to secure-start position (0/0/120)",
                           self.config["instrumentName"] + ".move(x=0, y=0, F=" + str(self.config["zSpeeds"][1]) + ")  # Move to origin (0/0/0)",
                           self.config["instrumentName"] + ".move(x=0, y=0, z=" + str(newZ) + ", F=" + str(self.config["zSpeeds"][1]) + ")  # secure-start", 
                           self.config["instrumentName"] + ".move(x=30, y=0, z=" + str(newZ) + ", F=" + str(self.config["xySpeeds"][1]) + ")  # Move in x direction to (30/0/230)",
                           self.config["instrumentName"] + ".move(x=30, y=30, z=" + str(newZ) + ", F=" + str(self.config["xySpeeds"][1]) + ")  # Move in y direction to (30/30/230)",
                           "#################################################"
                          ]
               
        elif selection == 1:  # initialization
            newZ = self.config["zSizeWorkingSpace"] - 10
            commandList = ["",
                           "# initialization",
                           self.config["instrumentName"] + ".move(z=" + str(newZ) + ", F=" + str(self.config["zSpeeds"][1]) + ")  # Retract tool to prevent collision with obstacles",
                           self.config["instrumentName"] + ".move(x=0, y=0, z="+ str(newZ) + ", F=" + str(self.config["zSpeeds"][1]) + ")  # Move to secure-start position (0/0/230)",
                           "##################################################"
                          ]
            
        elif selection == 2:  # mix3xY
            newZ = self.config["zSizeWorkingSpace"] - 10
            commandList = ["",
                           self.config["instrumentName"] + ".move(z=" + str(newZ) + ", F=" + str(self.config["zSpeeds"][2]) + ")", 
                           self.config["instrumentName"] + ".move(x=150, y=128, F=" + str(self.config["xySpeeds"][3]) + ")",
                           self.config["instrumentName"] + ".relative()", 
                           "for mixIndex in range(0, 3):",
                           "    " + self.config["instrumentName"] + ".move(y=-5, F=" + str(self.config["xySpeeds"][3]) + ")", 
                           "    " + self.config["instrumentName"] + ".move(y=5, F=" + str(self.config["xySpeeds"][3]) + ")", 
                           self.config["instrumentName"] + ".absolute()",
                           "###############################################"
                          ]
 
        elif selection == 3:  # pick up tip
            newZ = self.config["zSizeWorkingSpace"] - 40
            commandList = ["",
                           self.config["instrumentName"] + ".pick_up_tip()",
                           "###############################################"
                          ]
        
        elif selection == 4:  # drop tip
            newZ  = self.config["zSizeWorkingSpace"] - 10
            commandList = ["",
                           self.config["instrumentName"] + ".drop_tip()",
                           "###############################################"
                          ]
          
        elif selection == 5:  # waste_disposal
            newZ = self.panelLayout.layout.wasteObj[0].plateHight + 5
            commandList = ["",
                           self.config["instrumentName"] + ".move_to(" + self.panelLayout.layout.wasteObj[0].name + '["A1"])',
                           self.config["instrumentName"] + ".blow_out()",
                          "###############################################"
                          ]

        elif selection == 6:  # empty pump
            newZ = robot.VirtualRobot.currentZhight
            commandList = [self.config["instrumentName"] + ".blow_out()",
                           "###############################################" 
                          ]
                           
        elif selection == 7:  # wash1_clean3x. Dependent on tip type
            newZ = robot.VirtualRobot.currentZhight
            if abs(robot.VirtualRobot.currentTipHight - self.config["tipHight10uL"]) < 5:
                volume = 25.0
            elif abs(robot.VirtualRobot.currentTipHight - self.config["tipHight200uL"]) < 5 or abs(robot.VirtualRobot.currentTipHight - self.config["tipHight200uL"] - 100) < 5:
                volume = 350.0
            elif abs(robot.VirtualRobot.currentTipHight - self.config["tipHight1000uL"]) < 5: 
                volume = 1200.0
            else:
                volume = 0.0      
            commandList = ["",
                           "# wash1 clean3x",
                           self.config["instrumentName"] + ".mix(repetitions = 3, volume=" + str(volume) + ")",
                           "###############################################"
                          ]
            
        elif selection == 8:  # pipetMix3x. Dependent on tip type
            newZ = robot.VirtualRobot.currentZhight
            if abs(robot.VirtualRobot.currentTipHight - self.config["tipHight10uL"]) < 5:
                volume = 10.0
            elif abs(robot.VirtualRobot.currentTipHight - self.config["tipHight200uL"]) < 5 or abs(robot.VirtualRobot.currentTipHight - self.config["tipHight200uL"] - 100) < 5:
                volume = 200.0
            elif abs(robot.VirtualRobot.currentTipHight - self.config["tipHight1000uL"]) < 5: 
                volume = 1000.0
            else:
                volume = 0.0          
            commandList = ["",
                           "# mix by repeated uptake and dispension",
                           self.config["instrumentName"] + ".mix(repetitions = 3, volume=" + str(volume) + ")",
                           "###############################################"
                          ]

        command = "\n    ".join(commandList)
        if ((newZ is not None) and (newZ != robot.VirtualRobot.currentZhight)):
            robot.VirtualRobot.currentZhight = newZ
        self.GetParent().GetPage(1).pyotronextTextbox.WriteText(text=command)


        self.listBoxSpecialCommands.SetSelection(wx.NOT_FOUND)  # remove selection

    """The following buttons open the dialogs defined at the top and upon clicking OK in the dialog, send the method blocks as specified in the dialog."""        
    def onMoveXYbtn(self, moveXYbtnEvent):
        onMoveXYbtnDialog = dialogs.Move_xy_Dialog(parent = self, title = "Move xy Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if (onMoveXYbtnDialog.ShowModal() == wx.ID_OK):
            commandList = ["", 
                           self.config["instrumentName"] + ".move(" + str(onMoveXYbtnDialog.inputTextboxXY.GetValue()) + ", F=" + str(onMoveXYbtnDialog.inputTextboxSpeed.GetValue()) + ")", 
                           "################################################"
                          ]
            if (onMoveXYbtnDialog.radioboxAbsoluteRelativeMove.GetSelection() == 1):
                commandList.prepend(self.config["instrumentName"] + "relative()")
                commandList.append(self.config["instrumentName"] + "absolute()")
            command = "\n    ".join(commandList)
            self.GetParent().GetPage(1).pyotronextTextbox.WriteText(text=command)
        onMoveXYbtnDialog.Destroy       

   
    def onMoveZbtn(self, moveZbtnEvent):
        """move to raw z position given in textbox """
        inputTextboxZvalue = int(self.inputTextboxZ.GetValue())
        commandList = ["",
                       self.config["instrumentName"] + ".move(z=" + str(inputTextboxZvalue) + ", F=" + str(self.config["zSpeeds"][2]) + ")", 
                       "####################################################"
                      ]
        command = "\n    ".join(commandList)
        self.GetParent().GetPage(1).pyotronextTextbox.WriteText(text=command)

        robot.VirtualRobot.currentZhight = inputTextboxZvalue


    def onMoveTiptoPredefZbtn(self, moveTiptoPredefZbtnEvent):
        """Show dialog Dispense_Dialog. On OK, the target z position is 
        calculated based on target item hight and current pipette tip type. 
        Send the specified method block as 
        pub.sendMessage(topicName="methodBlockSent", gCodeText=...) """
        onMoveTiptoPredefZbtnDialog = dialogs.Move_Tip_to_Predef_z_Dialog(parent = self, title = "Move Tip to Predefined z Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if (onMoveTiptoPredefZbtnDialog.ShowModal() == wx.ID_OK):
            if (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 0):  # plate type = None
                item = None
                item.bottomZ = 0
                item.topZ = 0    
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 1):  #plate type = plate96Well
                itemName = "corning_96_wellplate_360ul_flat"
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 2):
                itemName = "opentrons_96_tiprack_10ul"
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 3):
                itemName = "opentrons_96_tiprack_20ul"
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 4):
                itemName = "opentrons_96_tiprack_300ul"
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 5):
                itemName = "opentrons_96_tiprack_1000ul"
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 6):
                itemName = "derandere_1_trash_832ml"
            elif (onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection() == 7):
                itemName = "opentrons_1_trash_850ml_fixed"

            definitionFileName = f"{itemName}.json" 
            definitionDirectory = currentScriptDirectory / "labware"
            definitionFilePath = definitionDirectory / definitionFileName
            try: 
                with definitionFilePath.open() as lDefFilehandler: 
                    labwareDict = json.load(lDefFilehandler)
            except OSError:
                wx.LogError("Cannot open file")
            print()
            itemPlateHight = labwareDict["wells"]["A1"]["z"]
            itemBottom= labwareDict["wells"]["A1"]["z"] - labwareDict["wells"]["A1"]["depth"]
            itemCenter=(itemPlateHight + itemBottom) / 2
            if (onMoveTiptoPredefZbtnDialog.radioBoxHight.GetSelection() == 0):  # maximum
                zSelected = self.config["zSizeWorkingSpace"] - robot.VirtualRobot.zOffset() -5 + int(onMoveTiptoPredefZbtnDialog.inputTextboxZoffset.GetValue())
            elif (onMoveTiptoPredefZbtnDialog.radioBoxHight.GetSelection() == 1):  # above
                zSelected = itemPlateHight + 5 + int(onMoveTiptoPredefZbtnDialog.inputTextboxZoffset.GetValue())
            elif (onMoveTiptoPredefZbtnDialog.radioBoxHight.GetSelection() == 2):  # center
                zSelected = itemCenter + int(onMoveTiptoPredefZbtnDialog.inputTextboxZoffset.GetValue())
            elif (onMoveTiptoPredefZbtnDialog.radioBoxHight.GetSelection() == 3):  # low
                zSelected = itemBottom + 3 + int(onMoveTiptoPredefZbtnDialog.inputTextboxZoffset.GetValue())
            elif (onMoveTiptoPredefZbtnDialog.radioBoxHight.GetSelection() == 3):  # bottom
                zSelected = itemBottom + int(onMoveTiptoPredefZbtnDialog.inputTextboxZoffset.GetValue())

            commandList = ["", 
                           "# Move to the predefined z-hight" + onMoveTiptoPredefZbtnDialog.itemChoices[onMoveTiptoPredefZbtnDialog.listBoxPlateType.GetSelection()] + "_[" + onMoveTiptoPredefZbtnDialog.zChoices[onMoveTiptoPredefZbtnDialog.radioBoxHight.GetSelection()] + "] + (" + onMoveTiptoPredefZbtnDialog.inputTextboxZoffset.GetValue() + ") mm",
                           self.config["instrumentName"] + ".move(z=" + str(zSelected) + ", F=" + str(self.config["zSpeeds"][2]) + ")",
                           "################################################"
                           ]
            command="\n    ".join(commandList)
            self.GetParent().GetPage(1).pyotronextTextbox.WriteText(text=command)

            robot.VirtualRobot.currentZhight = zSelected
            robot.VirtualRobot.currentPlateHight = itemPlateHight
        onMoveTiptoPredefZbtnDialog.Destroy       

    def onDispenseBtn(self, dispenseBtnEvent):
        """Show dialog Dispense_Dialog. On OK, send the specified method 
        block as pub.sendMessage(topicName="methodBlockSent", gCodeText=...) """
        onDispenseBtnDialog = dialogs.Dispense_Dialog(parent = self, title = "Dispense Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
##            mcd = labProcedures.dispense(self, V=onDispenseBtnDialog.inputTextboxDispense.GetValue(), mode=onDispenseBtnDialog.radioboxMode.GetSelection() == 0)
##            self.GetParent().GetPage(1).pyotronextTextbox.WriteText(text=mcd)

        if onDispenseBtnDialog.ShowModal() == wx.ID_OK:
            volumeString = onDispenseBtnDialog.inputTextboxDispense.GetValue()
            volume = float(volumeString)
            if volume >= 0.0:
                dispenseCommand= ".aspirate(" + volumeString + ")"
            elif volume < 0.0:
                dispenseCommand= ".dispense(" + str(0.0 - volume) + ")"
            if (onDispenseBtnDialog.radioboxMode.GetSelection() == 0):

                commandList = ["",
                               self.config["instrumentName"] + dispenseCommand,
                               "###################################################"
                               ]

            elif (onDispenseBtnDialog.radioboxMode.GetSelection() == 1):                 
                commandList = ["", 
                               "# Dispense and wipe at vessel wall",
                               self.config["instrumentName"] + ".relative()", 
                               self.config["instrumentName"] + ".move(z=-2, F=" + str(self.config["zSpeeds"][2]) + ")",
                               self.config["instrumentName"] + ".move(x=3, F=" + str(self.config["xySpeeds"][2]) + ")",
                               self.config["instrumentName"] + dispenseCommand, 
                               self.config["instrumentName"] + ".move(z=2, F=" + str(self.config["xySpeeds"][2]) + ")",
                               self.config["instrumentName"] + ".move(x=-3, F=" + str(self.config["xySpeeds"][2]) + ")", 
                               self.config["instrumentName"] + ".absolute()", 
                               "###################################################"
                               ]
            command = "\n    ".join(commandList)
            self.GetParent().GetPage(1).pyotronextTextbox.WriteText(text=command)

        onDispenseBtnDialog.Destroy


    def replaceLayout(self, old, currentLayoutFilePath):
        self.hBoxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)
        self.mainVboxSizer.Replace(old, self.hBoxSizer2)
        self.panelLayout = layout.PanelLayout(parent=self, layoutFilePath=currentLayoutFilePath)
        self.showLabwarePanels(parent=self.panelLayout, pyotroNextTextboxPage=1)

        self.mainVboxSizer.Layout()
    
    
    
    def onSetLayoutBtn(self, setLayoutBtnEvent):
        """Show dialog Set_SetLayout_Dialog and on OK update the panel 
        PanelSetNextCommand to show only the selected layout-panel as a child 
        (subpanel) within PanelSetNextCommand."""
        onSetLayoutBtnDialog = dialogs.Set_Layout_Dialog(parent = self, title = "Set Layout Wizard", style = (wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER))
        if onSetLayoutBtnDialog.ShowModal() == wx.ID_OK:
            selectedLayout = onSetLayoutBtnDialog.choiceLayout.GetSelection()
            if selectedLayout == 0:
                 layoutName = "defaultLayout"
            elif selectedLayout == 1:
                layoutName = "layout1"
            elif selectedLayout == 2:
                layoutName = "layout2"
            elif selectedLayout == 3:
                layoutName = "layout3"
            layoutFileName = f"{layoutName}.INI"
            layoutFileDirectory = currentScriptDirectory / "layouts"
            currentLayoutFilePath = layoutFileDirectory / layoutFileName


            """
            self.mainVboxSizer.Remove(self.hBoxSizer2)  # try Detach()
            self.panelLayout.Destroy()
            self.hBoxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)
#            self.Layout()  # update the layout of the panel PanelSetNextCommand
#            self.hBoxSizer2.Layout()  # update the layout of the panel PanelSetNextCommand

            self.panelLayout = layout.PanelLayout(parent=self, layoutFilePath=currentLayoutFilePath)
            self.showLabwarePanels(parent=self.panelLayout, pyotroNextTextboxPage=1)

            self.hBoxSizer2.Add(self.panelLayout, proportion=0, flag=(wx.LEFT | wx.RIGHT | wx.ALIGN_CENTER), border = 5)
            self.mainVboxSizer.Add(self.hBoxSizer2, proportion=0, flag=(wx.ALL | wx.ALIGN_CENTER), border=0)
#            self.mainVboxSizer.Add(self.hBoxSizer1, proportion=0, flag=(wx.ALL | wx.ALIGN_CENTER), border=0)
#            self.mainVboxSizer.Add(self.hBoxSizer2, proportion=0, flag=(wx.ALL | wx.ALIGN_CENTER), border=0)

            self.SetSizerAndFit(self.mainVboxSizer)
            """
            self.replaceLayout(self.hBoxSizer2, currentLayoutFilePath)
        onSetLayoutBtnDialog.Destroy

    def onAddItemBtn(self, addItemBtnEvent):
        newPlate=plate.PanelPlate(parent = self.panelLayout, definition=definitionChoice, IDnum = self.IDnum, slot=self.slotChoice)
        self.panelLayout.addPanelPlate(name = newplate.attributes.name, labwareObj = newplate)

